# IAC2 - Infrastructure as Code 2

 - Bereich: System- und Netzwerkarchitektur bestimmen 
 - Semester: 2

## Lektionen: 

* Präsenz: 20
* Virtuell: 20
* Selbststudium: 20


## Fahrplan:

Hier ist der aktualisierte Fahrplan mit der korrekten Verteilung des Selbststudiums für das 1. und 2. Semester.


| **Lektionen** | **Selbststudium**              | **Inhalt** | **Kompetenzband**                             | **Tools**                             |
|----------------------------------------|------------------------------------|--|-------------------------------------------------|--------------------------------------|
| 24 Lektionen                            | 12 Lektionen                         | Automatisierung von Konfigurationen              | b) | Ansible                     |Spezifische Tools, Bewertungskriterien |
| 16 Lektionen                            | 8 Lektionen                         | Kombinieren von Tools                           |  e) |  GitLab CI, Docker            |

## Voraussetzungen:

Module IAC1,AWS/Azure

## Dispensation


## Technologien

- [multipass.run](https://multipass.run/)
- [AWS Academy](https://awsacademy.instructure.com/login/canvas)
- [Gitlab-CICD](https://docs.gitlab.com/ee/ci/)

## Methoden: 

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe: 

Automatisieren,Ansible,Pipelines,CI/CD

## Lerninhalte
 - Ansible Inventories kennenlernen, erstellen und benutzen
 - Ansible Variablen,Facts kennenlernen, benutzen
 - Ansible Playbooks,Roles,Tasks,Handler kennenlernen, erstellen und benutzen
 - Ansible Galaxy kennenlernen
 - Ansible Integration mit Cloud-Providern
 - Einfache Pipelines erstellen und verstehen auf Gitlab

## Übungen und Praxis

 - Ansible: Erstellen von Ansible-Inventories 
 - Ansible: Erstellen von Inventories mit Hilfe von Cloud-Provider-Plugins
 - Ansible: Erstellen von Playbooks 
 - Ansible: Einfache Applikationen Deployen
 - Ansible: Modifikation von Systemkonfigurationen
 - Ansible: Modifikation von SystemkonfigurationenKomplizierte Applikationen erstellen
 - CICD: Erstellen und deployen von Docker Image
 - CICD: AWS-Codepipeline


## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Laborübungen



## Lehrmittel:

- [Bash](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)
- [Ansible Core](https://docs.ansible.com/ansible-core/devel/index.html)
- [Cloud-Init](https://cloudinit.readthedocs.io/en/latest/)
- [Gitlab-CICD](https://docs.gitlab.com/ee/ci/)

## Hilfsmittel:

- Rahmenlehrplan 
