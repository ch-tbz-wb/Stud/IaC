# IAC1 - Infrastructure as Code 1

 - Bereich: System- und Netzwerkarchitektur bestimmen
 - Semester: 1

## Lektionen: 

* Präsenz: 40
* Virtuell: 30
* Selbststudium: 40

## Fahrplan:


| **Lektionen** | **Selbststudium**              | **Inhalt** | **Kompetenzband**                             | **Tools**                             |
|----------------------------------------|------------------------------------|-------------------------------------------------| --|--------------------------------------|
| 40 Lektionen                           | 20 Lektionen                         | Automatisierung von Installationen               | a) | Cloud-Init, Shell Scripting   |
| 14 Lektionen                           | 8 Lektionen                         | Automatisierung von Konfigurationen              | b) | Ansible, Chef, Ansible                 |
| 4 Lektionen                           | 3 Lektionen                         | Evaluation von Tools                            | c) |Verschiedene Cloud-Automatisierungstools, Dokumentation |
| 8 Lektionen                           | 6 Lektionen                         | Automatisierung von Migrationsprozessen          | d) | AWS Migration Hub, CloudEndure, Google Cloud Migration |
| 4 Lektionen                            | 3 Lektionen                         | Überblick über die Toollandschaft                | f) | Analyse der Tools, Präsentationen     |

## Voraussetzungen:

Linux-Grundkenntnisse

## Dispensation

## Technologien

- [multipass.run](https://multipass.run/)
- [AWS Academy](https://awsacademy.instructure.com/login/canvas)

## Methoden: 

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe: 

Automatisieren,Cloud-Init,Ansible,Bash,YAML

## Lerninhalte
 - Toollandschaft in der Infrastrure as Code kennenlernen 
 - Cloud-Init Module kennenlernen und einsetzen
 - Cloud-Init uns Sicherheit
 - Cloud-Init Internals
 - Grundlagen in BASH-Programmierung
 - bessere Skripte programmieren
 - Erste Schritte in Ansible

## Übungen und Praxis

 - Provisionieren von Virtuellen Rechnern mit Cloud-Init
 - Deployen von Komplexen Applikationen mit Cloud-Init und Bash
 - Cloud-Init sicher machen
 - Cloud-Init-Ablauf modifizieren
 - Abläufe mit Bash-Skripten programmieren
 - Ansible aufsetzen



## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen, Laborübungen



## Lehrmittel:  

- [Bash](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)
- [Ansible Core](https://docs.ansible.com/ansible-core/devel/index.html)
- [Cloud-Init](https://cloudinit.readthedocs.io/en/latest/)

## Hilfsmittel:

- Rahmenlehrplan 



