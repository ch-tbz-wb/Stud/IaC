# Role die auf Ubuntu mariadb-server aufsetzt

[playbook.yml](./playbook.yml) ist eine ganz simples playbook das diese Role mariadb benutzt  
[roles/mariadb](./roles/mariadb/) Role die das aufsetzen und eine Basic Konfig erzeugt und allenfalls mariadb durchstarted wenn config angepasst wird.  
[roles/mariadb/tasks/main.yml](./roles/mariadb/tasks/main.yml) Tasks zum installieren und erstellen von konfig und service starten  
[roles/mariadb/templates//50-server.cnf.j2](./roles/mariadb/templates/50-server.cnf.j2) Konfig file template  
[roles/mariadb/handlers/main.yml](./roles/mariadb/handlers/main.yml) Handler zum durchstarten von mariadb service.  