# Auftrag 6: Installieren von MySQL

Der Kern von Ansible ist das geschickte "programmieren" von Playbooks, was in [Working with Playbooks](https://docs.ansible.com/ansible/latest/playbook_guide/index.html) sehr ausführlich beschrieben wird.

Hier geht es darum ein Playbook zu erstellen, welches zuerst mysql installiert und konfiguriert. Weil wir das reusable machen möchten , erstellen wir eine Role `mysql`, welche 
- die Richtigen Packages für mariadb installiert 
- sicherstellt, dass mariadb richtig konfiguriert ist, d.h. erstellt ein ein Jinja-Template um Port und Netzwerk-Adresse zu konfigurieren (siehe /etc/mysql/mariadb.conf.d/50-server.cnf nachdem ihr mariadb installiert habt). 
- erstellt ein Handler welcher Maria-DB durchstartet wenn die Konfiguration geaendert wird.
- sicherstellt, dass der Service laueft

Um einen guten Start zu haben was Roles tun gibt es folgende beiden Seiten die ich empfehlen kann:
  - [Official Ansible Documentation on Roles](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
  - [How to Use Ansible Roles to Abstract your Infrastructure Environment Tutorial by Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-use-ansible-roles-to-abstract-your-infrastructure-environment)

[Einfache Loesung](./Loesung/Readme.md)

Wenn ihr das alles gemacht habt, und euer Playbook funktioniert, dann schaut euch noch folgendes Repo von Faustin Lammer an [https://github.com/fauust/ansible-role-mariadb](https://github.com/fauust/ansible-role-mariadb)

Es gibt auch ein [Video 19:52](https://www.youtube.com/watch?v=CV8-56Fgjc0) von ihm, in welchem er seine Ansible Role demonstriert.
