Immer wenn man das erste mal auf einen neuen Rechner einloggt per ssh wird folgende Warnung gezeigt:
```
(ansible) Armins-M2:~ ado$ ansible -i inv.ini -m ping all
The authenticity of host '192.168.64.64 (192.168.64.64)' can't be established.
ED25519 key fingerprint is SHA256:NU5q5S5jaA4wRd1B8g6Bbcn4NIJE4gP7DtoOu/piybg.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? 
```
Dann müsste man jeden dieser Fingerprints akzeptieren. Das ist aber mühsam bei mehreren Ansinle-Nodes. Darum gibt Ansible die Möglichkeit, diese Warnung zu ignorieren und den Fingerprint automatisch zu akzeptieren. Einfach eine Variable `ANSIBLE_HOST_KEY_CHECKING` setzen und schon ignoriert Ansible die Fingerprint Warnung:
```
export ANSIBLE_HOST_KEY_CHECKING=False
```
Dann das Kommando wieder ausführen
```
(ansible) Armins-M2:~ ado$ export ANSIBLE_HOST_KEY_CHECKING=False
(ansible) Armins-M2:~ ado$ ansible -i inv.ini -m ping all
192.168.64.64 | UNREACHABLE! => {
    "changed": false,
    "msg": "Failed to connect to the host via ssh: Warning: Permanently added '192.168.64.64' (ED25519) to the list of known hosts.\r\nubuntu@192.168.64.64: Permission denied (publickey).",
    "unreachable": true
}
```
Dann kommt ihr zum nächsten Problem. Ihr müsst den richtigen Key verwenden um ohne Passwort einloggen zu dürfen.



