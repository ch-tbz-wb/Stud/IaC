Die Lösung besteht aus einem  

- Inventory file als [ini-File](./inventory.ini)
- Gruppenvariablen in einer Verzeichnisstruktur [group_vars](./group_vars/)
- Hostvariablen in einer Verzeichnisstruktur [host_vars](./host_vars/)

Man könnte das ganze auch in einem Yaml-File machen.

Dabei habe ich default-Passwörter für `webserver` und `dbserver` gesetzt.
Diese habe ich für die Kunden `migros` und `nationalbank` überschrieben, nicht aber für `coop`.
Dann habe ich noch einen Web und Db-Server der `nationalbank` mit anderen Passwörtern beglückt.

Schaut euch die resultierende Konfiguration mit folgenden Kommandos an:

```bash
for i in `egrep -v '=|\[' inventory.ini|sort -u`
do
echo $i
ansible-inventory -i inventory.ini --host $i
done
```