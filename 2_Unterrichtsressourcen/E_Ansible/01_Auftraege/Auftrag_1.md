# Auftrag 1: Ein einfaches Inventory und Playbook erstellen

## Inventory
Erstelle ein Inventory für 3 Server, für die ich dir per Teams die Zugansdaten zur Verfügung stelle. Erstelle eine Gruppe `mymanagednodes`. Setze die Variable `ansible_user`, damit ansible automatische mit dem richtigen User einloggt. Erstelle je ein Inventory-File im ini-Format und eines im yaml-Format.

[Lösung Inventory](Auftrag_1_Loesung_Inventor.md)

Teste deine Inventories mit:
```
ansible --list-hosts -i <Inventoryfile> all 
ansible --list-hosts -i <Inventoryfile> mymanagednodes
```
Es sollte zweimal denselben Output liefern.

## Connection-Check

Versuche, ob du die Hosts im Inventory erfolgreich per Ansible ansprechen kannst. Benutze das Ansible Module `ping` dazu
```
ansible -i <Inventoryfile> -m ping <Gruppenname>
```
**Was passiert?**  
**Warum?**

Versuche das Problem zu lösen.

**Hast du das erste Problem gelöst?**

[SSH-Fingerprint-Trick](./Auftrag_1_fingerprint_trick.md)
Versuche es noch einmal.

**Was passiert?**  
**Warum?**

Benutzen sie den SSH-Private-Key `iackey` den ich ihnem per Teams zur verfügung stelle um sich zu einzuloggen. 
```
ansible --private-key iackey -i <Inventoryfile> -m ping <Gruppenname> 
```

# Playbook erstellen

Erstelle ein Playbook, um genau ein File mit deinem Namen in `/` zu erstellen.

Benutze ganz einfach das Ansible Module [`file`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#examples)

Führe das Playbook aus:
```
ansible-playbook  --private-key iackey -i <Inventoryfile> <playbookfile>
```

warum funktioniert es nicht?

Füge die richte Option zum obigen Kommando hinzu, dass es funktioniert.

[Lösung Playbook](Auftrag_1_Loesung_Playbook.md)