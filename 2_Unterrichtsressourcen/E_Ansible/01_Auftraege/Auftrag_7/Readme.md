# Auftrag 7: AWS Plugin für Inventory

Zuerst sollt ihr AWS-CLI, boto, boto3 mit pip installieren. Benutzt die folgende [Anleitung](https://www.freecodecamp.org/news/ansible-manage-aws/). **Ihr müsst keine S3 buckets machen!!! Wir werden nur das Inventory Plugin benutzen. Zum testen des AWS-CLI könnt ihr `aws ec2 describe-vpcs` benutzen**


Jetzt können wir das [AWS-Inventory-Plugin](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html) benutzen. 

Als erstes muss man einfach mal ein File `inventory.aws_ec2.yml` (**Achtung, die Endung `aws_ec2.yml` ist wichtig**) mit dem Inhalt:
```yml
plugin: amazon.aws.aws_ec2
regions:
  - us-east-1
```

Jetzt sollte das Kommando:
```bash
ansible-inventory -i inventory.aws_ec2.yml --list
```
ein Json mit allen VMs die ihr deployed habt zurück geben.

Jetzt sollt ihr Ansible-Gruppen mit Hilfe von `keyed_groups` und `groups` anhand von `tags`, `security_groups` erstellen. (Siehe [AWS-Inventory-Plugin](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html)). 

## Tips

Ihr könnt alle möglichen [Filter](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_filters.html) benutzen.

Testen ob filter etwas sinnvolles tun könnt ihr mit `ansible -m debug`. Ein Beispiel:

Ihr habt im inventory folgendes:
```
groups:
  webserver: security_groups|json_query("[].group_name") is contains ("webserver")
```
und wisst für welche hosts `security_groups|json_query("[].group_name") is contains ("webserver")` wirklich `true` zurück gibt. Dann führt ihr einfach
```bash
ansible -i inventory.aws_ec2.yml -m debug -a 'msg={{security_groups|json_query("[].group_name") is contains ("webserver")}}' all
```
aus und bekommt:
```
ec2-3-73-53-62.eu-central-1.compute.amazonaws.com | SUCCESS => {
    "msg": true
}
ec2-3-68-108-64.eu-central-1.compute.amazonaws.com | SUCCESS => {
    "msg": true
}
```
zurück. -> Dann wisst ihr welche Rechner zur Gruppe `webserver` gehören.

[Lösung](./ansible_aws/Readme.md)
