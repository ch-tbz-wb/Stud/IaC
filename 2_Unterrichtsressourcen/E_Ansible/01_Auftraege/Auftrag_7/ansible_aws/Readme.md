# Loesung
mit `pip` instlliert ihr AWS-CLI,boto,boto3 und ansible-core:
```bash
pip install awscli
pip install boto boto3
pip install ansible-core
```
Dann konfiguriert ihr AWS-CLI mit Accesskeys:
```bash
aws configure
```
Dann erstellt ihr ein Inventory-File mit [`inventory.aws_ec2.yml`](./inventory.aws_ec2.yml). **Wichtig** Die Endung des Filenamens muss `.aws_ec2.yml` sein, sonst wird das AWS-Invetory-Plugin nicht funktionieren.

Dann kontrolliert ihr ob das Inventory funktioniert:
```bash
ansible-inventory -i inventory.aws_ec2.yml --list
```
Der Output sollte etwas wie das folgende sein:
```json
    {
    "_meta": {
        "hostvars": {
        ...
                }
        }
    },
    "all": {
        "children": [
            "ansiblegroup",
            "aws_ec2",
            "securitygroup_webserver",
            "tag_Name",
            "ungrouped",
            "webserver"
        ]
    },
    "ansiblegroup": {
        "hosts": [
            "ec2-3-68-108-64.eu-central-1.compute.amazonaws.com",
            "ec2-3-73-53-62.eu-central-1.compute.amazonaws.com"
        ]
    },
    "aws_ec2": {
        "hosts": [
            "ec2-3-68-108-64.eu-central-1.compute.amazonaws.com",
            "ec2-3-73-53-62.eu-central-1.compute.amazonaws.com"
        ]
    },
    "securitygroup_webserver": {
        "hosts": [
            "ec2-3-68-108-64.eu-central-1.compute.amazonaws.com",
            "ec2-3-73-53-62.eu-central-1.compute.amazonaws.com"
        ]
    },
    "tag_Name": {
        "hosts": [
            "ec2-3-68-108-64.eu-central-1.compute.amazonaws.com",
            "ec2-3-73-53-62.eu-central-1.compute.amazonaws.com"
        ]
    },
    "webserver": {
        "hosts": [
            "ec2-3-68-108-64.eu-central-1.compute.amazonaws.com",
            "ec2-3-73-53-62.eu-central-1.compute.amazonaws.com"
        ]
    }
}
```

Jetzt seht ihr am Schluss für jede Gruppe welche Server dazugehoeren.
