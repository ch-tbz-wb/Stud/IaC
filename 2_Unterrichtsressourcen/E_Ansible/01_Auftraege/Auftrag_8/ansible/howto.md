- install ansible:
    ```
    python3 -m venv /tmp/venv_ansible
    . /tmp/venv_ansible/bin/activate
    pip install ansible
    ```
- update inventory file with ips of hosts run the playbook on:
    ```
    echo [ip of server] > inventory
    ```
- use ssh-copy-id to install your private key on the server

- if you have never accessed a server to check you need to disable SSH-Host-Key-Checking:
  ```
  export ANSIBLE_HOST_KEY_CHECKING=False
  ```
- run the playbook on hosts with:
    ```
    ansible-playbook -i inventory playbook.yml
    ```
    expected output:
    ```
    PLAY [all] **********************************************************************************************************************************************

    TASK [Gathering Facts] **********************************************************************************************************************************
    ok: [192.168.64.49]

    TASK [armin_pub_key] ************************************************************************************************************************************
    changed: [192.168.64.49]

    TASK [marcello_armin_key] *******************************************************************************************************************************
    changed: [192.168.64.49]

    TASK [file "{{item}}"] **********************************************************************************************************************************
    changed: [192.168.64.49] => (item=/home/ubuntu/docs)
    changed: [192.168.64.49] => (item=/home/ubuntu/docs/images)
    changed: [192.168.64.49] => (item=/home/ubuntu/src)
    changed: [192.168.64.49] => (item=/home/ubuntu/bin)
    changed: [192.168.64.49] => (item=/home/ubuntu/lib)

    .
    .
    .
    .
    TASK [user : armin_pub_key] *****************************************************************************************************************************
    changed: [192.168.64.49]

    TASK [user : marcello_armin_key] ************************************************************************************************************************
    changed: [192.168.64.49]

    PLAY RECAP **********************************************************************************************************************************************
    192.168.64.49              : ok=288  changed=287  unreachable=0    failed=0    skipped=2    rescued=0    ignored=0   

    ```
