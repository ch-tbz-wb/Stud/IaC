# Lösung

Das Playbook [playbook.yml](./playbook.yml) benutzt die Rollen [site](./roles/site/) und [user](./roles/user/) um die tasks zu erfüllen.

Um das Playbook zu benutzen koennt ihr wie im [Howto](howto.md) beschrieben vorgehen.

