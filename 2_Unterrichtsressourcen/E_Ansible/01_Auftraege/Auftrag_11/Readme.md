# Auftrag

Erstellt eine VM mit Ansible. Dabei sollt ihr soviel der nötigen Informationen die es braucht um eine VM zu erstellen (Subnet-ID, VPC-ID, AMI-ID, SSH-Key,...) dynamisch mit Ansible-Modulen abfragen und diese dann benutzen um die VM aufzusetzen.

# Virtual-Environment erstellen

Installieren von Ansible und AWS-Modulen
```
python -m venv ansible_aws
. ansible_aws/bin/activate
pip install boto3 ansible-core
ansible-galaxy collection install amazon.aws

```

# Credentials anpassen

Als erstes müsst ihr die Credentials vom Lerner-Lab ein credentials File eintragen.
```
vi ~/.aws/credentails
```
und allenfalls noch das file `~/.aws/config` anpassen:
```
[default]
region = us-east-1
```

# Übergeordnete Resourcen in AWS-Konsole erzeugen

Bei jeder Resource (VPC,Subnetze, SSH-Keys) die ihr in AWS erzeugt, stellt sich die Frage, ob ihr diese in der AWS-Konsole vorbereiten wollt, oder gleich mit einen Ansible-Module erstellen wollt. Zum Start würde ich zum Beispiel ein VPC in der Konsole erstellen. Später könnt ihr dann dieses auch mit dem Ansible-Module [amazon.aws.ec2_vpc_net](https://docs.ansible.com/ansible/latest/collections/amazon/aws/ec2_vpc_net_module.html) machen. 

# Playbook erzeugen das auf `localhost` läuft

Alle Ansible-AWS-Module können nicht auf einer AWS-Instanz ausgeführt werden, die es gar noch nicht gibt. Darum benutzen wir als Ansible-Node unseren eigenen Rechner also `localhost`.

```
- name: awsplay
  hosts: localhost
  connection: local

```

Falls ihr einen Cloud-Rechner zur verfügung habt, auf welchen die richtigen Python Module (boto3 braucht es ganz sicher) installiert sind, könnt ihr auch diesen anstatt `localhost` benutzen.

# Nötige Infos auslesen

Um zu wissen, was es zum erstellen einer VM braucht, solltet ihr Beispiele des Moduls [amazon.aws.ec2_instance](https://docs.ansible.com/ansible/latest/collections/amazon/aws/ec2_instance_module.html#examples) anschauen.

Diese Infos wie z.B. vpcid und subnetid müsst ihr mit AWS-Modulen zusammentragen. Hier die Beispiele für vpcid, und subnetid:

```
  - name: Get all VPCs in region
    amazon.aws.ec2_vpc_endpoint_info:
        aws_region: "{{aws_region}}"
    register: existing_endpoints
  - name: Set the vpcid
    set_fact:
      vpcid: "{{existing_endpoints.vpc_endpoints[0].vpc_id}}"
  - debug:
      msg: "{{existing_endpoints}}"
  - name: Get subnetid
    amazon.aws.ec2_vpc_subnet_info:
      filters:
        availability-zone: "{{aws_region}}a"
    register: subnets
  - set_fact:
      subnetid: "{{subnets.subnets[0].subnet_id}}"  

  ...
  ...
```

# Security Group und VM erstellen

Jetzt müsst ihr noch die Security-Group aufsetzen und die VM:

```
- name: example using security group rule descriptions
    amazon.aws.ec2_security_group:
      name: "ssh and http"
      description: sg with rule descriptions
      vpc_id: "{{vpcid}}"
      rules:
        - proto: tcp
          ports:
          - 22
          - 80
          cidr_ip: 0.0.0.0/0
          rule_desc: allow all on port 80
    register: sg

  - name: Launching EC2 instances
    amazon.aws.ec2_instance:
      profile: default
      key_name: vockey
      security_group: "{{ sg.group_name }}"
      instance_type: t3.micro
      image_id: "{{ aws_ami_id }}"
      state: present
      wait: yes
      wait_timeout: 300
      region: "{{ aws_region }}"
      tags:
         Name: "{{ vmname }}"
      detailed_monitoring: no
      vpc_subnet_id: "{{ subnetid }}"
      network:
        assign_public_ip: yes
```

# Zusatzaufgaben
- Erzeugt 3 VMS test1-3
- Erzeugt verschiedene VMs mit demselben playbook nur indem ihr geschickt Variablen definiert (anstatt einer Liste von vmnames koennt ihr gerade auch eine Liste von `dicts` machen in welchen ihr alle angaben zur VM unterbringt):
  - Eine Instanz mit ubuntu 22.04, des Types `t3.micro`
  - Eine Instanz mit Centos, des Types `t3.small`
  - ...


[Lösung](./playbook_aws_instance.yml)