# Backup and Cleanup

Benutzt das Playbook aus der Theorie um herauszufinden was der typische "Output" von Ansible-Modulen ist. Ihr werdet merken, dass nicht alle Module dieselben Output-Felder haben.

Benutzt für folgende Task jeweils `register: variablename` und das Modul `debug`, um den Output auszugeben.

- Fügt mit dem Ansible-Module `lineinfile` im `myapp.conf` zusätzlich eine Zeile mit folgendem Inhalt hinzu:

    ```
    debuglevel=100
    ```

- Kopiert mit dem Ansible-Module `copy` ein völlig neues File `myapp.conf` mit einem Total neuen Inhalt in den Konfigordner. 

[Lösung](./Loesung.md)