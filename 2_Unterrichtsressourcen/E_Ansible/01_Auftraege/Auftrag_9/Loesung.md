# Lösung

Ein mögliches playbook findet ihr [hier](./playbook.yml)

Es generiert in etwa folgenden output:

```
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [Testplaybook] **********************************************************************************************************************************************************************************

TASK [Gathering Facts] *******************************************************************************************************************************************************************************
ok: [localhost]

TASK [just create the directory and the first configfile] ********************************************************************************************************************************************
changed: [localhost]

TASK [make backup file] ******************************************************************************************************************************************************************************
changed: [localhost]

TASK [lineinfile] ************************************************************************************************************************************************************************************
changed: [localhost]

TASK [debug] *****************************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": {
        "backup": "",
        "changed": true,
        "diff": [
            {
                "after": "",
                "after_header": "myapp/myapp.conf (content)",
                "before": "",
                "before_header": "myapp/myapp.conf (content)"
            },
            {
                "after_header": "myapp/myapp.conf (file attributes)",
                "before_header": "myapp/myapp.conf (file attributes)"
            }
        ],
        "failed": false,
        "msg": "line added"
    }
}

TASK [copy] ******************************************************************************************************************************************************************************************
changed: [localhost]

TASK [debug] *****************************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": {
        "changed": true,
        "checksum": "b233fff6895caab811badbfb32e9cc5edb11ae93",
        "dest": "myapp/myapp.conf",
        "diff": [],
        "failed": false,
        "gid": 20,
        "group": "staff",
        "md5sum": "7d82c7cc6db9e7d088a3c2be5e813a9e",
        "mode": "0644",
        "owner": "ado",
        "size": 40,
        "src": "/Users/ado/.ansible/tmp/ansible-tmp-1700229552.727971-9354-159944453050091/source",
        "state": "file",
        "uid": 501
    }
}

TASK [get the list of files ordered by timestamp] ****************************************************************************************************************************************************
changed: [localhost]

TASK [debug] *****************************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": {
        "changed": true,
        "cmd": "ls -1t myapp/myapp.conf.*",
        "delta": "0:00:00.003726",
        "end": "2023-11-17 14:59:13.175241",
        "failed": false,
        "msg": "",
        "rc": 0,
        "start": "2023-11-17 14:59:13.171515",
        "stderr": "",
        "stderr_lines": [],
        "stdout": "myapp/myapp.conf.20231117T145912\nmyapp/myapp.conf.20231117T145846\nmyapp/myapp.conf.20231117T145817",
        "stdout_lines": [
            "myapp/myapp.conf.20231117T145912",
            "myapp/myapp.conf.20231117T145846",
            "myapp/myapp.conf.20231117T145817"
        ]
    }
}

TASK [remove files all files except the last 5] ******************************************************************************************************************************************************
skipping: [localhost]

TASK [debug] *****************************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": {
        "changed": false,
        "results": [],
        "skipped": true,
        "skipped_reason": "No items in the list"
    }
}

PLAY RECAP *******************************************************************************************************************************************************************************************
localhost                  : ok=10   changed=5    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0  
```

