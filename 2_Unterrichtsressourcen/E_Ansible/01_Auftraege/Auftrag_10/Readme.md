# Ignorieren von Fehlern

Ignoriert alle Fehler beim letzten Task `remove all files except the last {{numberofbackups}}` im Playbook des [Auftrags 9](../Auftrag_9/playbook.yml), da dieser Task ja nicht kritisch ist, wenn er **failed**. Dann gibts halt ein Backup zuviel.

# Aufgrund des "Outputs" definieren, wann ein Task **failed** ist

Im [Auftrags 9](../Auftrag_9/playbook.yml) benutzen wir im ersten Task das Modul "shell" um am Anfang ein Directory zu erzeugen und dann ein leeres Konfigurationsfile zu erstellen. Übungshalber möchte ich, dass ihr das mit 2 Tasks erledigt:

```
  - name: just create the directory
    shell:
      cmd: "mkdir $(dirname {{configfile}})"
  - name: create empty file
    shell:
      cmd: "touch {{configfile}}"
```

Benutzt `failed_when` um folgendes zu definieren.
- Der Task `just create the directory` soll **failed** sein, wenn das Verzeichnis nicht erzeugt werden kann, aber noch nicht existiert. `mkdir` gibt `File exists` auf den `stderr` zurück, wenn das Directory schon existiert. 
  

# Aufgrund des "Outputs" definieren, wann ein Task **changed** ist

Der Status changed soll nur `true` sein wenn auch wirklich das Verzeichnis erzeugt wurde.
Benutzt `changed_when`, um dies zu erreichen.

# einen Block definieren

Definiert für die beiden Tasks `lineinfile` und `copy` einen Block.

# Fehler im Block abfangen.

Fangt Fehler im Block ab, indem ihr die "letzte" Version des Config-Files zurückkopiert.

Benutzt einen `rescue:`-Block dazu und schaut, dass allfällige `Handler` auch im Fehlerfall laufen.

# Tasks im Block auf jeden falls ausführen

Jetzt geht es darum, dass wir auf jeden Fall die alten Backups löschen möchten

Dazu sollt ihr die Tasks zum löschen im `always:`-Block unterbringen.

[Lösung](./playbook.yml)

