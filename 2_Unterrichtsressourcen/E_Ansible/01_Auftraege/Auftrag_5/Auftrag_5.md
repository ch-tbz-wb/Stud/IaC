# Auftrag 5

Erstelle ein Playbook, das auf jedem Node, der zur Gruppe *webserver* gehört:

- einen Apache-Webserver installiert ([ansible.builtin.package](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html)),
- einen virtuellen Server `www.kunde.com` konfiguriert,
- diesen virtuellen Server durch Benutzername und Passwort schützt.

Das Playbook soll den Benutzernamen, das Passwort und den virtuellen Servernamen aus Variablen des Inventars beziehen.

Für das Erstellen des virtuellen Hosts kann eine Datei mit einem [Jinja-Template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html) verwendet werden. Nutze Google oder ChatGPT, um herauszufinden, wie man bei Apache einen virtuellen Host erstellt.

Verwende das [htpasswd-Modul](https://docs.ansible.com/ansible/latest/collections/community/general/htpasswd_module.html), um die Passwortdatei zu erstellen.

Nutze das Inventar aus [Auftrag 3](../Auftrag_3/Auftrag_3.md) und gestalte das Playbook so, dass es für alle vier Webserver funktioniert.

[Lösung Auftrag 5](Auftrag_5_Loesung.md)