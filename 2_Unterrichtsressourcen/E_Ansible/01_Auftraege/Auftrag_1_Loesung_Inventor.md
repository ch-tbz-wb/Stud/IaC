Das Inventory im ini-Format koennte so aussehen:
```ini
[mymanagednodes]
managednode1 ansible_host=3.75.247.201 ansible_user=ubuntu
managednode2 ansible_host=3.75.247.202 ansible_user=ubuntu
managednode3 ansible_host=3.75.247.203 ansible_user=ubuntu
```

Das gleich Inventory im yaml-Format sieht dann so aus:
```yaml
mymanagednodes:
    hosts:
        managednode1:
            ansible_host: 3.75.247.201
            ansible_user: ubuntu
        managednode2:
            ansible_host: 3.75.247.202
            ansible_user: ubuntu
        managednode3:
            ansible_host: 3.75.247.203
            ansible_user: ubuntu
```