# Umgebung fuer Auftrag herstellen

Hier wird eine Umgebung mit Terraform in einen AWS Lernerlab erstellt

## Instruktionen

```
ssh-keygen -f ~/.ssh/iackey -t ed25519 -N ""

vi ~/.aws/credentials #Use the creds from Learnerlab
terraform init
terraform plan
terraform apply
```
