provider "aws" {
  region = "us-east-1"
}


resource "aws_vpc" "default" {
  cidr_block = "10.10.1.0/24"
}

resource "aws_subnet" "default" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = "10.10.1.0/26"
  availability_zone = "us-east-1a"
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.default.id
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

}

resource "aws_route_table_association" "rt_ass" {
  subnet_id = aws_subnet.default.id
  route_table_id = aws_route_table.rt.id
}
resource "aws_network_acl" "nacl" {
  vpc_id = aws_vpc.default.id

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
}

resource "aws_network_acl_association" "nacl_assoc" {
  subnet_id      = aws_subnet.default.id
  network_acl_id = aws_network_acl.nacl.id
}

resource "aws_security_group" "ssh_sg" {
  name        = "ssh_sg"
  description = "Security group for SSH access"
  vpc_id      = aws_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_key_pair" "iackey" {
  key_name   = "iackey"
  public_key = file("~/.ssh/iackey.pub")
}

data "aws_ssm_parameter" "ubuntu_ami" {
  name = "/aws/service/canonical/ubuntu/server/22.04/stable/current/amd64/hvm/ebs-gp2/ami-id"
}


resource "aws_instance" "ubuntu_instance" {
  count         = 3
  ami           = data.aws_ssm_parameter.ubuntu_ami.value
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"  

  key_name      = aws_key_pair.iackey.key_name
  subnet_id     = aws_subnet.default.id

  vpc_security_group_ids = [aws_security_group.ssh_sg.id]

  associate_public_ip_address = true

  tags = {
    Name = "ubuntu_iac_${count.index + 1}"
  }
}

output "IP" {
  value=aws_instance.ubuntu_instance.*.public_ip
}