# Ansible und Azure

Ich nehme vorweg, dass ihr ziemlich frustriert sein werdet.

## Credentials erstellen auf Azure-Cloud-Shell

Hier müsst ihr eine App in Azure registrieren. Das kann **nicht** mit dem TBZ-Edu Account geschehen, da dieser keine Berechtigung dazu hat. Erstellt auf https://portal.azure.com einen neuen Account und registriert euch als Education account. Dann kriegt ihr 100 Franken guthaben und könnt damit testen.

```
AZURE_SUBSCRIPTION_ID=$(az account show --query id --output tsv)
```

```
az ad sp create-for-rbac --name ansibleuser --role contributor --scopes /subscriptions/$AZURE_SUBSCRIPTION_ID
Creating 'contributor' role assignment under scope '/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
The output includes credentials that you must protect. Be sure that you do not include these credentials in your code or check the credentials into your source control. For more information, see https://aka.ms/azadsp-cli
{
  "appId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "displayName": "ansibleuser",
  "password": "1Uxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  "tenant": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```
** Wichtig **: Der Output und die AZURE_SUBSCRIPTION_ID muss an einen sicheren Ort am besten verschüsselt (KeePass oder so) gespeichert werden . Diese Informationen braucht ihr dann weiter unten um von der VM auf Azure zugreifen zu können.


Bei '--scopes /subscriptions/.....' kann man auch noch auf Resourcegruppen einschränken wenn solche existieren.



## Ansible für Azure auf der Cloud-Shell

Der erste kleine Auftrag schein machbar:

Versucht anhand der [Microsoft Dokumentation](https://learn.microsoft.com/en-us/azure/developer/ansible/getting-started-cloud-shell?tabs=ansible) mit Ansible die ersten Schritte zu machen.

Ziel wäre es eine Resource-Gruppe mit Ansible zu erstellen.

## Ansible für Azure in VM installieren

Auch das sollte eigentlich ja kein Problem sein. Es gibt ja eine [offizielle Microsoft Dokumentation](https://learn.microsoft.com/en-us/azure/developer/ansible/install-on-linux-vm?tabs=azure-cli#install-ansible-on-an-azure-linux-virtual-machine)



## Ansible für Azure in einem Virtual Env auf einer Ubuntu VM installieren

**Achtung**: Es geht nur auf Intel-basierenden-VMs, ARM-VMs also Apple-Mx Chips funktionieren nicht.

Installieren von Ansible in venv auf einer Ubuntu VM

```
sudo apt-get update
sudo apt-get install python3-venv
python3 -m venv ansible_azure
source ansible_azure/bin/activate
pip install --upgrade pip
pip install 'ansible[azure]'
pip install -r /home/ubuntu/ansible_azure/lib/python3.10/site-packages/ansible_collections/azure/azcollection/requirements-azure.txt
```

Jetzt setzt ihr einfach die Credentials mit folgenden Kommandos:
```
export AZURE_SUBSCRIPTION_ID="477fab00-b3a6-......."
export AZURE_CLIENT_ID="a6823d77-6ca5-......."
export AZURE_SECRET="ysa....."
export AZURE_TENANT="128af66e-....."
```
Die richtigen Values habt ihr bei [Credentials erstellen](#credentials-erstellen-auf-azure-cloud-shell) erzeugt und irgendwo sicher abgespeichert.

Jetzt könnt ihr folgendes Playbook benutzen um eine VM zu erzeugen:
```
- name: Create Azure VM
  hosts: localhost
  connection: local
  tasks:
  - name: Create resource group
    azure_rm_resourcegroup:
      name: myResourceGroup
      location: eastus
  - name: Create virtual network
    azure_rm_virtualnetwork:
      resource_group: myResourceGroup
      name: myVnet
      address_prefixes: "10.0.0.0/16"
  - name: Add subnet
    azure_rm_subnet:
      resource_group: myResourceGroup
      name: mySubnet
      address_prefix: "10.0.1.0/24"
      virtual_network: myVnet
  - name: Create public IP address
    azure_rm_publicipaddress:
      resource_group: myResourceGroup
      allocation_method: Static
      name: myPublicIP
    register: output_ip_address
  - name: Public IP of VM
    debug:
      msg: "The public IP is {{ output_ip_address.state.ip_address }}."
  - name: Create Network Security Group that allows SSH
    azure_rm_securitygroup:
      resource_group: myResourceGroup
      name: myNetworkSecurityGroup
      rules:
        - name: SSH
          protocol: Tcp
          destination_port_range: 22
          access: Allow
          priority: 1001
          direction: Inbound
  - name: Create virtual network interface card
    azure_rm_networkinterface:
      resource_group: myResourceGroup
      name: myNIC
      virtual_network: myVnet
      subnet: mySubnet
      public_ip_name: myPublicIP
      security_group: myNetworkSecurityGroup
  - name: Create VM
    azure_rm_virtualmachine:
      resource_group: myResourceGroup
      name: myVM
      vm_size: Standard_DS1_v2
      admin_username: azureuser
      ssh_password_enabled: false
      ssh_public_keys:
        - path: /home/azureuser/.ssh/authorized_keys
          key_data: "ssh-rsa ............"
      network_interfaces: myNIC
      image:
        offer: CentOS
        publisher: OpenLogic
        sku: '7.5'
        version: latest
```
Ihr müsst nur noch euren SSH-Public-key eintragen (ssh_public_keys->key_data). Es muss ein rsa key sein.

Jetzt koennt ihr mit folgendem Kommando das Playbook laufen lassen:
```
ansible-playbook playbook.yml
```

## Review

Das ganze war für mich recht ernüchternd, da keine einzige Doku die ich gefunden habe, die wirklich auf Anhieb funktioniert hat.

BTW: Es gibt eine ganze Menge Issues die reported sind. 

**Migrosoft hat wieder mal was geaendert ohne Ankündigung**: https://github.com/ansible-collections/azure/issues/1260#issuecomment-1723531288 

Aber Migrosoft gibt ja den Hinweis, dass man einfach die Alpha Version von Ansible für Azure benutzen soll:
https://github.com/ansible-collections/azure/issues/1260#issuecomment-1723531288

Aber was würdet ihr tun, wenn ihr das Produktiv benutzt? Naja, ich würde zu AWS wechseln.



