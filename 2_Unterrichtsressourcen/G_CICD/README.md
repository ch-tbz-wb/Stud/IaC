
# Continuous Integration (CI) und Continuous Deployment (CD)

[TOC]

## Einführung

### Was ist Continuous Integration (CI)?

Continuous Integration (CI) ist eine Praxis in der Softwareentwicklung, bei der Codeänderungen regelmäßig und automatisch in ein gemeinsames Repository integriert werden. Das Hauptziel von CI ist es, Konflikte und Fehler frühzeitig im Entwicklungsprozess zu erkennen. Das bedeutet, dass Entwickler häufig kleine Codeänderungen vornehmen und diese in das Hauptrepository zusammenführen. Dadurch werden Konflikte minimiert und die Qualität des Codes verbessert.

### Was ist Continuous Deployment (CD)?

Continuous Deployment (CD) ist der Schritt, der auf CI folgt. Bei CD werden die Änderungen, die im CI-Prozess überprüft und als stabil befunden wurden, automatisch in die Produktionsumgebung bereitgestellt. Dies ermöglicht eine schnelle und häufige Auslieferung von Softwareanwendungen.

### Warum sind CI und CD wichtig?

- Fehlererkennung: CI hilft, Fehler frühzeitig im Entwicklungsprozess zu entdecken, was die Qualität des Codes erhöht.

- Schnelle Bereitstellung: CD ermöglicht es, neue Funktionen und Aktualisierungen in kurzen Intervallen bereitzustellen, was die Reaktionsfähigkeit auf Kundenbedürfnisse verbessert.

- Konsistenz: Durch die Automatisierung werden konsistente und reproduzierbare Bereitstellungsprozesse sichergestellt.

- Zeitersparnis: Die Automatisierung von CI/CD-Prozessen reduziert den manuellen Aufwand und beschleunigt die Bereitstellung.

### Werkzeuge für CI/CD

Es gibt eine Vielzahl von Tools und Plattformen, die CI/CD unterstützen, darunter Jenkins, Travis CI, GitLab CI/CD, CircleCI, und viele andere.

### Hands-On

#### Auftrag 1: Erste Pipeline auf Gitlab
Anhand vom 
[Tutorial to Build and deploy a Webserver-Image](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-continuous-deployment-pipeline-with-gitlab-ci-cd-on-ubuntu-18-04)
solltet ihr eure erste GitLab-CI/CD-Pipeline erstellen. 

**Herausforderungen** 

Es werden euch ein paar Steine in den Weg gelegt:
- Prerequisites müsst ihr zuerst erfüllen:
  - Benutzt AWS-Academy-Lab um eine Ubuntu-VM aufzusetzen auf welcher folgendes läuft:
    - ein Gitlab-Runner  
    - ein Dockerserver
- Software Versionen im Tutorial sind nicht aktuell -> Es gibt bestimmt Fehler wenn ihr einfach Copy-Paste macht. Diese müsst ihr dann beheben -> Lernkurve....

## GITLAB-CI-CD

### Theorie: Gitlab-Runner & Gitlab-Executor

*Gitlab-Runner* ist ein Stück Software, das sich mit `https://gitlab.com` verbindet um zu schauen ob es etwas zu tun gibt. 

Dazu muss der Runner zuerst bei Gitlab registriert werden. Das geschieht in zwei Schritten:
1. Auf der Webseite einen neuen Runner erfassen -> Man bekommt ein Registrierungs-Token.
2. Wo immer die [gitlab-runner Software installiert](https://docs.gitlab.com/runner/install/) ist, einfach mit `gitlab-runner register` mit dem Token und den richtigen Optionen registrieren. 

Wenn es jetzt Jobs gibt, welche auf die Tags und die Konfiguration des Runners passen wird der erste auf diesem "gescheduled". Der Gitlab-Runner holt sich diesen Jobs ab und führt die nötigen Schritte durch und gibt logs and gitlab.com zurück. Dabei benutzt er einen *Executor*. 

Pro Gitlab-Runner wird während des Registrierungsprozesses genau ein Executor definiert (vielleicht wären auch mehrere möglich, aber ich habe keine offizielle Doku gefunden).

Es gibt verschiedene Arten von Executoren. Diese sind unter anderen:
- Shell
- Docker
- Kubernetes

Vorteile von Kubernetes und Docker-Executoren sind vorallem, dass für verschiedene Jobs immer verschiedene Container-Instanzen gestartet werden. Dadurch gibt es weniger bis keine Sicherheitsbedenken, wenn derselbe Gitlab-Runner von verschiedenen Pipelines benutzt wird. 

#### Debug the Executor

#### Direkt in der VM
Wenn man eine docker Executor benutzt, kann man direkt auf der VM auf welcher der Gitlab-Runner läuft einloggen und direkt in den docker container Verbinden und dort den Fehler oder so suchen:
```bash
ubuntu@ip-172-31-17-247:~$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED              STATUS              PORTS           NAMES
1f806a4e1e49   5b912308540a   "dockerd-entrypoint.…"   49 seconds ago       Up 48 seconds       2375-2376/tcp   runner-yb6sxz-nn-project-51466953-concurrent-0-d8c198f18b1a651e-build
010bb1559f0d   5b912308540a   "dockerd-entrypoint.…"   About a minute ago   Up About a minute   2375-2376/tcp   runner-yb6sxz-nn-project-51466953-concurrent-0-d8c198f18b1a651e-docker-0
ubuntu@ip-172-31-17-247:~$ docker exec -ti 1f806a4e1e49 /bin/sh
/ # ps -ef
PID   USER     TIME  COMMAND
    1 root      0:00 /bin/sh
   14 root      0:00 /bin/sh
   57 root      0:00 sleep 300
   58 root      0:00 /bin/sh
   65 root      0:00 ps -ef
/ # cd /builds/armindoerzbachtbz/demopipeline
demopipeline.tmp/  demopipeline/
/ # cd /builds/armindoerzbachtbz/demopipeline
/builds/armindoerzbachtbz/demopipeline # ls
Dockerfile           README.md            index.html           overview.drawio      overview.drawio.png
/builds/armindoerzbachtbz/demopipeline # docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
/builds/armindoerzbachtbz/demopipeline # docker image ls
REPOSITORY                                                 TAG        IMAGE ID       CREATED              SIZE
registry.gitlab.com/armindoerzbachtbz/demopipeline/armin   792d8948   1d8fbd93bba0   About a minute ago   133MB
registry.gitlab.com/armindoerzbachtbz/demopipeline/armin   latest     1d8fbd93bba0   About a minute ago   133MB
/builds/armindoerzbachtbz/demopipeline # 
```

#### Ueber das Webterminal
Auf der Webseite von Gitlab ist auch noch folgende Möglichkeit beschrieben. **Leider funktioniert sie nicht zuverlässig**:

Um einen Job zu debuggen gibt es die Möglichkeit den gitlab-runner so zu konfigurieren, dass er ein Web-Terminal direkt im Executor startet. Dazu müssen folgende Konfigurationen im /etc/gitlab-runner/config.toml File vorgenommen werden:
```
[session_server]
  listen_address = "[::]:8093" #  listen on all available interfaces on port 8093
  advertise_address = "<VM-IP-wo-Gitlab-runner-laueft>:8093"
  session_timeout = 1800

```
Danach muss gitlab-runner neu gestartet werden.

Dann muss der Port 8093 auch noch in der Firewall, AWS-Security-Group,... so freigeschaltet werden, dass irgendein Rechner von gitlab.com sich auf diesen Port verbinden kann.

Wichtig ist auch, dass ihr keine `Shared runners` aktiviert habt:
![runners](./resourcen/bild_runners.png)

Danach kann das Feature [Debugging a Job in a Webterminal](https://docs.gitlab.com/ee/ci/interactive_web_terminal/#debugging-a-running-job) benutzt werden.

Wie ich festgestellt habe, ist das Web-Terminal leider nicht sehr zuverlässig:
  - Es funktioniert manchmal gar nicht (Überlastung der gitlab.com Platform)
  - Das Terminal hängt des öfteren. Reload der Web-Seite hilft meistens.

### Auftrag 2: Debuggen

Wir werden jetzt mit der Pipeline von der Einführung ein paar Sachen ausprobieren.

#### Debugging in Webterminal oder direkt in der VM

Verbindet mit dem Executor auf beider Arten:

**für Webterminal:**
- Fügt die Konfig für das Webterminal in euren gitlab-runner ein
- Fügt im Job `publish` ein `sleep 600` als letztes Kommando ein,  damit ihr Zeit habt um zu debuggen...
- Startet das Web-Terminal

**für direkt in der VM**:
- Fügt im job `publish` ein `sleep 600` als letztes Kommando ein,  damit ihr Zeit habt um zu debuggen...
- Benutzt in der VM des gitlab-runners `docker exec -ti <id_des_containers> /bin/sh` um in den richtigen container zu verbinden.

Dann könnt ihr folgendes rausfinden.
  - welche IP-Adresse hat der Container des Executors
  - welche IP-Adresse hat der $DOCKER_HOST
  - Schaut euch an welche `IMAGE ID` euer generiertes Docker Image hat.
  - Findet heraus wie was es für Variabeln im Environment gesetzt sind
  - Schaut was in $CI_JOB_TOKEN steht.
- Fügt im job `deploy` ein `sleep 600` als letztes Kommando ein und löscht im `publish` den `sleep 600`

- Verbindet über das Web-Terminal oder mit `docker exec ...` und ..
  - Findet heraus in welchem container ihr euch jetzt gerade befindet. Tip: `docker ps` auf eurer VM ausfuehren.
  - Welche IP hat dieser Container?
  - Findet heraus wo im Container das File mit dem Private Key vom user deployer gespeichert ist. (TIP: Das ist das File das ihr in Gitlab mit `ID_RSA` definiert habt und hat den Namen `ID_RSA`)
- Schaut euch das Directory mal an und auch noch das Parent-Directory davon.

### Auftrag 3: Separieren von Web-Server und gitlab-runner auf 2 VMs

Wir werden die Loesung in Auftrag eins wie folgt erweitern:

- Wir erstellen eine VM auf welcher dann der Webserver container laufen soll:

| Loesung Auftrag 1 | Ziel Auftrag 3 |
| - | - |
| ![](./resourcen/overview-Auftrag1.drawio.png) | ![](./resourcen/overview-Auftrag3.drawio.png) |



Erstellt eine zusätzliche VM, auf welcher ihr Docker installiert.

Erstellt auf dieser VM einen User `deployer` welcher zur Gruppe `docker` gehört.

Installiert den richtigen SSH-Key im `authorized_keys` file.

Macht Änderungen an Variabeln und Pipeline so, dass jetzt der Deploy-Job das Image auf der neuen VM ausführt.

[Lösung](https://gitlab.com/armindoerzbachtbz/demopipeline)

### Theorie: GITLAB-Pipelines

Gitlab-Pipelines werden erzeugt, indem man einfach ein File `.gitlab-ci.yml` im Repo erzeugt.

Ungefährer Aufbau:

```yaml
# Alle default settings wie z.B.
default:
  image: alpine:latest
  services: 
    -docker:latest
# Includes von anderen Files
include: './gitlab-ci-template.yml'

# Stages die definiert sind. Jobs im gleichen Stage werden parallel abgearbeitet, wenn nichts anderes definiert ist.

stages:
  - build
  - test
  - deploy

# Globale Variabeln, die dann während den jobs mit $.... benutzt werden koennnen.
variables:
  SERVICE_NAME: "my-service"
  SERVER_IP: "devserver"

# Workflows. Hier kann man Variabeln in Abhängigkeit von Commit-Tags,Branches, ... definieren, sodass z.B. die SERVER_IP wo was deployed werden soll anders ist ja nach branch, oder aber auch bestimmen ob die Pipeline überhaupt laufen soll.
workflow:
  - rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
        variables:
          SERVER_IP: "productionserver"  # Override globally-defined SERVER_IP
    - if: $CI_COMMIT_TITLE =~ /-draft$/  # Das verhindert das die Pipeline laeuft 
      when: never                        # bei einem Commit mit Titel der auf "-draft" endet. 
    - when: always # when sagt, dass die Pipeline geführt wird. Und zwar immer wenn kein if es verhindert.

# Jobs sind alles was nicht eines der obigen 5 Keywoerter (default,include, stage,....) ist, ist ein Job.

job1:
...

job2:
...

# Der Job compile_java soll auf dem docker image ubuntu:latest ausgeführt werden und gehoert zum Stage build
compile_java:
  image: ubuntu:latest
  stage: build
  script:
    - apt update && apt install openjdk make
    - cd $CI_PROJECT_DIR
    - make


job4:
...

```
Die möglichen Keywords werden in [.gitlab-ci.yml keyword reference](https://docs.gitlab.com/ee/ci/yaml/) beschrieben.

Es gibt viele [vordefinierte Variabeln](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) die ihr auch benutzen könnt.

Ob eine Pipeline bei jedem Commit, bzw. Push oder nur bei bestimmten Aktionen laufen soll kann man mit [Workflows](https://docs.gitlab.com/ee/ci/yaml/workflow.html) bestimmen

Mit [Rules](https://docs.gitlab.com/ee/ci/jobs/job_control.html) bestimmt man unter welchen umständen welcher Job laufen soll oder eben nicht.

Man kann auch bestimmte Dateien die in einem Job hergestellt werden als [Artifact](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html) an die nächsten Jobs übergeben.
Die hergestellten Files muessen mit relativen Pfadangeben referenziert werden. Diese werden dann in ein zip-File gepackt und dann in Jobs im nachfolgenden Stage im aktuellen Verzeichnis wieder ausgepackt. Das Auspacken geschieht automatisch.


### Auftrag 4: Testing innerhalb von Pipeline
Benutzt die Pipeline von der Einführung um folgendes zu tun.

- Erzeugt einen Test-Job der nachdem deploy Job den Webserver testet.

- Er soll zur Stage `deploy` gehören, aber erst ausgeführt werden wenn deploy korrekt durchgelaufen ist. 






