# Unterrichtsressourcen 

*Überblick über die Unterrichtsressourcen*

*Ressourcen in Unterordnern ablegen und in dieser Übersichtsseite sinnvolle Hinweise und Verlinkungen ergänzen*

* [Übersicht Infrastructure as Code (IaC)](A_Uebersicht_IaC/)
* [Toolslandschaft IaC](B_Toollandschaft_IaC/)
* [Automatisieren mit Bash](C_Bash/)
* [Cloud-Init](D_Cloud-Init/)
* [Ansible](E_Ansible/)
* [Basics mit AWS-CLI, Azure-CLI](F_Cloud_CLIs/)
* [CI/CD-Pipelines](G_CICD/)

