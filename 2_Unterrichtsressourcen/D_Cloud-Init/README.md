# Cloud-Init

[[_TOC_]]

Cloud-Init ist ein Tool, das eine VM beim ersten Booten mit Hilfe von Meta-Daten so konfiguriert, dass sie vom Netzwerk erreichbar ist.  
Der Cloud-Provider (Vendor-Data) und/oder der User (User-Data) kann noch zusätzliche Konfigurationen und Software-Installationen definieren.  
Cloud-Init läuft zu 100% auf der VM ab.   
Cloud-Init wird eigentlich nur durch die von der Datasource zur Verfügung gestellten Daten gesteuert, welche während des Bootvorgangs *gepullt* werden (Meta-Daten,Vendor-Daten,User-Daten).  
Auch wird bei einigen Cloud-Providern Cloud-init schon im OS-Image das installiert wird, angepasst.

![Bild zu cloudinit](./cloud-init.drawio.png)

## Cloud-Init first steps

Wir werden die ersten Schritte mit Cloud-Init machen. Dazu werden wir eine Umgebung brauchen um Cloud-Init Scripts auszuprobieren. Wir werden [Multipass](https://multipass.run/) benutzen, welches in diesem [Video auf Englisch ab Minute 1:45 bis ca. 6:30](https://www.youtube.com/watch?v=16sphi3VZ-I) ausführlich erklärt wird und ein bisschen weniger ausführlich in diesem [Video auf Deutsch ab Minute 5:00 bis ca. 9:00](https://www.renefuerst.eu/schnelle-ubuntu-vms-mit-multipass/). Falls ihr multipass noch nicht kennt schaut euch eines der Videos an, damit ihr wisst wie ihr ein VM mit cloud-init aufsetzen könnt.

[Cheatsheet zu multipass](../Z_Cheatsheets/multipass)
### Kompetenzen

B4.6  	Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:2)

### Lernziele / Taxonomie 

- Ich kann eine einfache Cloud-Init Konfiguration erstellen und diese mit multipass deployen.

### Hands-on

[VM mit einfacher Cloud-Init-Konfiguration erstellen](./01_Auftraege/10_VM_mit_einfacher_cloudinit_config.md)


### Links

[Cloud-Init Examples](https://cloudinit.readthedocs.io/en/latest/reference/examples.html)

[Cloud-Init Modul Referenz (das Handbuch)](https://cloudinit.readthedocs.io/en/latest/reference/modules.html)


## Cloud-Init Logfiles

Cloud Init wird bei jedem reboot der VM wieder aufgerufen. Dabei schreibt es immer in die folgenden beiden Logfiles:

- **/var/log/cloud-init.log**: Hier kommt der Debugging output von den Cloud-Init-Skripten rein. Das ist wo wir die wichtigsten Infos finden
- **/var/log/cloud-init-output.log**: Hier kommt der output von Kommands rein. Meistens nicht so interessant

### Kompetenzen

B4.6  	Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:2)

### Lernziele / Taxonomie 

- Ich kann in den Logfiles Infos finden, wo die Cloud-Info Informationen cached.
- Ich kann Fehler finden.
- Ich weiss welche Stages dass es beim Cloud-Init gibt
- Ich weiss was eine Datasource ist.

### Hands-on

[Logfileaufträge](./01_Auftraege/20_logfiles.md)


### Links

[Cloud-Init Examples](https://cloudinit.readthedocs.io/en/latest/reference/examples.html)

[Cloud-Init Modul Referenz (das Handbuch)](https://cloudinit.readthedocs.io/en/latest/reference/modules.html)


## Cloud-Init Internals (Part 1)

Wie Cloud-Init aufgebaut ist erfahrt ihr in folgendem
[Einstiegs-Video](https://www.youtube.com/watch?v=exeuvgPxd-E) (ihr könnt die Werbung für Linode überspringen. Das Ende ist bei 2:20). In diesem Tutorial wird eigentlich erklärt, wie die verschiedenen Cloud-Provider Cloud-Init für den End-User also uns vorkonfigurieren.

[Link zu der cloud-init-Modul-Referenz welche im Video gezeigt wird](https://cloudinit.readthedocs.io/en/latest/reference/modules.html)

Wenn ihr noch genauer erfahren wollt wie cloud-init funktioniert kann ich euch [Understanding Cloud Init von leftasexcercise.com](https://leftasexercise.com/2020/04/12/understanding-cloud-init/comment-page-1/?unapproved=18958&moderation-hash=98502ae42d0bc3c994b5c4a9b57dcf09#comment-18958) empfehlen. Vielleicht wollt ihr aber zuerst an die [Hands-on](./README.md#hands-on-2) machen.

Vielleicht wollt ihr

### Kompetenzen

- B4.7  	Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (KN:3)

### Lernziele / Taxonomie 

- Ich kann ein bestehendes Tool kritisch analysieren.
- Ich kenne Möglichkeiten verschiedene Informationsquellen zu kombinieren (Source-Code, Dokumenationen, Tutorials)

### Transfer

- Ich verstehe wie Cloud-Init bei den Cloud-Providern vorkonfiguriert wird.

### Hands-on

 [Internals Aufträge](./01_Auftraege/30_internals_part1.md)


## Cloud-Init Module

Jetzt kommen wir wieder zurück zu den cloud-init Skripten als User-Data.
Darin kann man alle Module die im Konfigurations-File `/etc/cloud/cloud-cfg` sind, benutzen.

Für Cloud-Init gibt es verschiedene [Module](https://cloudinit.readthedocs.io/en/latest/reference/modules.html).



Für einige dieser Module gibt es gute [Beispiele](https://cloudinit.readthedocs.io/en/latest/reference/examples.html).


### Kompetenzen

- B4.7  	Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (KN:3)
- B4.6  	Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:3)

### Lernziele / Taxonomie 

- Ich kenne die wichtigsten Module die Cloud-Init bietet und kann diese benutzen.

### Hands-on

[Apache-Wordpress-PHP Auftrag](01_Auftraege/40_apache_php_wordpress_cloudinit.md)

### Links

[Cloud-Init Examples](https://cloudinit.readthedocs.io/en/latest/reference/examples.html)

[Cloud-Init Modul Referenz (das Handbuch)](https://cloudinit.readthedocs.io/en/latest/reference/modules.html)

## Cloud-Init Internals (Part2)

[Ablauf von Cloud-Init](02_Theorie/10_cloudinit_ablauf_beschreibung.md)



### Kompetenzen

- B4.7  	Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (KN:3)

### Lernziele / Taxonomie 

- Ich kann ein bestehendes Tool kritisch analysieren.
- Ich kenne Möglichkeiten verschiedene Informationsquellen zu kombinieren (Source-Code, Dokumenationen, Tutorials)

### Transfer

- Ich verstehe wie Cloud-Init bei den Cloud-Providern vorkonfiguriert wird.

### Hands-on

[Internals Aufträge](./01_Auftraege/30_internals_part1.md)

## Cloud-Init: Base64 Encoding

Falls ein "grösseres" Skript als File direkt im File `user-data.yml` von Cloud-Init mitgeben werden sollen, dann gibt es sehr oft Probleme, dass die Skript Syntax auch eine Bedeutung als cloud-init Syntax hat und in cloud-init "falsch" interpretiert werden könnte.
Um das zu verhindern ist es sinnvoll ein solches Skript Base64-encodiert im `user-data.yml` File einzufügen.

Als erstes muss man dafür das Skript, hier `skript.sh` Base64-encodieren.
Dazu gibt es auf Unix-Systemen das Kommando `base64`, welches so benutzt werden kann:
```
ado@ubuntu:~$ base64 skript.sh
IyEvYmluL2Jhc2gKCmNhdCA8PEVPRgojIE5ldHdvcmsgc2VydmljZXMsIEludGVybmV0IHN0eWxl
CiMKIyBVcGRhdGVkIGZyb20gaHR0cHM6Ly93d3cuaWFuYS5vcmcvYXNzaWdubWVudHMvc2Vydmlj
ZS1uYW1lcy1wb3J0LW51bWJlcnMvc2VydmljZS1uYW1lcy1wb3J0LW51bWJlcnMueGh0bWwgLgoj
CiMgTmV3IHBvcnRzIHdpbGwgYmUgYWRkZWQgb24gcmVxdWVzdCBpZiB0aGV5IGhhdmUgYmVlbiBv
ZmZpY2lhbGx5IGFzc2lnbmVkCiMgYnkgSUFOQSBhbmQgdXNlZCBpbiB0aGUgcmVhbC13b3JsZCBv
ciBhcmUgbmVlZGVkIGJ5IGEgZGViaWFuIHBhY2thZ2UuCiMgSWYgeW91IG5lZWQgYSBodWdlIGxp
c3Qgb2YgdXNlZCBudW1iZXJzIHBsZWFzZSBpbnN0YWxsIHRoZSBubWFwIHBhY2thZ2UuCgp0Y3Bt
dXgJCTEvdGNwCQkJCSMgVENQIHBvcnQgc2VydmljZSBtdWx0aXBsZXhlcgplY2hvCQk3L3RjcApl
Y2hvCQk3L3VkcApkaXNjYXJkCQk5L3RjcAkJc2luayBudWxsCmRpc2NhcmQJCTkvdWRwCQlzaW5r
IG51bGwKc3lzdGF0CQkxMS90Y3AJCXVzZXJzCkVPRgo=
```

Der Output von `base64 skript.sh` kann dann per Copy/Paste ins `user-data.yml` eingefügt und richtig eingerückt werden:

```
write_files:
- encoding: b64
  content: | 
    IyEvYmluL2Jhc2gKCmNhdCA8PEVPRgojIE5ldHdvcmsgc2VydmljZXMsIEludGVybmV0IHN0eWxl
    CiMKIyBVcGRhdGVkIGZyb20gaHR0cHM6Ly93d3cuaWFuYS5vcmcvYXNzaWdubWVudHMvc2Vydmlj
    ZS1uYW1lcy1wb3J0LW51bWJlcnMvc2VydmljZS1uYW1lcy1wb3J0LW51bWJlcnMueGh0bWwgLgoj
    CiMgTmV3IHBvcnRzIHdpbGwgYmUgYWRkZWQgb24gcmVxdWVzdCBpZiB0aGV5IGhhdmUgYmVlbiBv
    ZmZpY2lhbGx5IGFzc2lnbmVkCiMgYnkgSUFOQSBhbmQgdXNlZCBpbiB0aGUgcmVhbC13b3JsZCBv
    ciBhcmUgbmVlZGVkIGJ5IGEgZGViaWFuIHBhY2thZ2UuCiMgSWYgeW91IG5lZWQgYSBodWdlIGxp
    c3Qgb2YgdXNlZCBudW1iZXJzIHBsZWFzZSBpbnN0YWxsIHRoZSBubWFwIHBhY2thZ2UuCgp0Y3Bt
    dXgJCTEvdGNwCQkJCSMgVENQIHBvcnQgc2VydmljZSBtdWx0aXBsZXhlcgplY2hvCQk3L3RjcApl
    Y2hvCQk3L3VkcApkaXNjYXJkCQk5L3RjcAkJc2luayBudWxsCmRpc2NhcmQJCTkvdWRwCQlzaW5r
    IG51bGwKc3lzdGF0CQkxMS90Y3AJCXVzZXJzCkVPRgo=  
  owner: ubuntu:ubuntu
  path: /home/ubuntu/skript.sh
  permissions: '0755'
```

## Cloud-Init Configuration Validation und Debugging

In der Dokumentation von Cloud-init gibt es einige nützliche [Howto-Guides](https://cloudinit.readthedocs.io/en/latest/howto/index.html). In diesen findest du unter anderen folgende 2 sehr nützlichen Guides:

[Validate my user data](https://cloudinit.readthedocs.io/en/latest/howto/debug_user_data.html): Wie kann ich User-Data Syntax validieren bevor ich eine VM aufsetze (Es braucht eine Linux VM dazu)

[How to debug cloud-init](https://cloudinit.readthedocs.io/en/latest/howto/debugging.html): Für folgende 4 Probleme wird ein Vorgehen erklaert:
    - I can’t log in to my instance
    - Cloud-init did not run
    - Cloud-init ran, but didn’t do what I want it to
    - Cloud-init never finished running

### Hans-On

- [Cloud-Init-Validation](./01_Auftraege/31_cloudinitvalidation.md)
- [Cloud-Init-Debugging](./01_Auftraege/32_cloudinitdebugging.md)

## Cloud-Init und Zero-Trust

Zero-Trust ist ein Konzept, bei dem darauf geachtet wird, dass niemandem getraut wird, also auch nicht dem Cloud-Provider. Wenn man das zu Herzen nimmt, muss man sich gedanken machen ob Cloud-init nach der Initial-Installation einer VM noch trauen soll. Dabei geht es um 2 Sachen:

1. Ein Cloud-Provider kann jederzeit seine Vendor-Data ändern und damit nachträglich konfigurationen vornehmen.
2. Ein Cloud-Provider hat zugriff auf die User-Data und kann darin hinterlegte Credentials auslesen und diese missbrauchen.

Wie kann man das verhindern?

In diesem Kapitel möchte ich darauf eingehen, und euch eine Möglichkeiten aufzeigen, wie man Zero-Trust in dieser Beziehung Implementieren kann. Security ist mühsam, aber nötig.

### Kompetenzen

- B4.7          Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (KN:3)
- B4.6          Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:3)

### Lernziele / Taxonomie

- Ich kann ein bestehendes Tool kritisch analysieren.
- Ich kenne Möglichkeiten verschiedene Informationsquellen zu kombinieren (Source-Code, Dokumenationen, Tutorials)
- Ich kann verschiedene Tools geschickt miteinander kombinieren

### Transfer

- Ich verstehe wie Cloud-Init sicherer gemacht werden kann.
- Ich verstehe die Sicherheitsaspekte von Cloud-Init

### Hands-on

Ich habe dies in 6 kleine Aufträge unterteilt.
In den Aufträgen 2-5 geht es darum 4 kleine Bash-Skripte zu schreiben, die Cloud-Init auf eine sichere weise mit Gitlab verbinden.

![Bild](./01_Auftraege/X_gitresourcen/Zeichnung_Cloud-init-Zero-Trust.drawio.png)


### Auftrag 1: Cloud-Init disabeln

[Auftrag](./01_Auftraege/50_cloud_init_disabeln.md)

### Auftrag 2: kurzfristig gültige Credentials erstellen

[Auftrag](./01_Auftraege/51_erstellen_creds_gitlab.md)

### Auftrag 3: Credentials benutzen um User-Data für Cloud-Init zu erstellen

[Auftrag](./01_Auftraege/52_ausfuellen_user-data-template.md)

### Auftrag 4: Deployen der VM

[Auftrag](./01_Auftraege/53_vm_erstellen.md)

### Auftrag 5: Löschen der Credentials

[Auftrag](./01_Auftraege/54_loeschen_cred.md)

### Auftrag 6: Macht ein skript das alles zusammenfuegt

### Musterlösung
[Link zu Git-Repo https://gitlab.com/armindoerzbachtbz/cloudinit_zerotrust](https://gitlab.com/armindoerzbachtbz/cloudinit_zerotrust)


## Cloud-Init: Standard-Module

In diesem kleinen Abschnitt möchte ich, dass ihr die [Standard-Module](https://cloudinit.readthedocs.io/en/latest/reference/modules.html) von Cloud-Init genauer anschaut. Dabei geht es mir vorallem um folgende Module:

- ssh: Generieren und Deployen von SSH-Host-Keys und Authorized=Keys
- bootcmd: Befehle ganz am Anfang des Bootvorgangs ausführen
- runcmd: Befehle am Ende des Bootvorgangs ausführen
- ca_certs: Installierte Zertifikate verwalten für Tools wie curl, wget, openssl
- keyboard: Keyboard Settings
- locale: Sparche, Datumsformate,..
- ntp: Network Time Protokoll konfigurieren
- packages: Packages nachinstallieren
- power_state: Reboot, Halt, Shutdown nach erfolgreicher Installation.
- resolv_conf: DNS-Client Konfiguration
- vendor_data: Vendor-Data abschalten
- hostname: Hostname Konfigurieren
- timezone: Zeitzone Konfigurieren
- users: User, Gruppen und Sudo verwalten.
- write_files: Dateien installieren (allenfalls mit Templates arbeiten)

Es gibt noch ein paar zusätzliche Distributionsspezifiesche Tools )z.B. Spacewalk, zipper, yum, apt). Diese schauen wir nur an, wenn es wirklich mal nötig sein sollte.

Dann gibt es ein paar Module die Konfigurationsmanagement-Tools wie Ansible, Puppet, Chef integrieren. Diese schauen wir zu einen späteren Zeitpunkt, wenn wir genau diese Tools behandeln.

Damit das nicht alles nur Theorie bleibt habe ich euch einen [Auftrag](./01_Auftraege/60_cloudinit_module.md) vorbereitet, in welchem ihr alle diese Module benutzen werdet.

### Kompetenzen

- B4.7          Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (KN:3)
- B4.6          Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (KN:3)

### Lernziele / Taxonomie

- Ihr kennt die Standard-Module von Cloud-Init
- Ihr könnt diese in einer einfachen Version anwenden

### Hands-On

[Auftrag](./01_Auftraege/60_cloudinit_module.md)


## Cloud-Init: Jinja-Templates und Meta-Daten

Manchmal ist es wichtig, dass man beim Aufsetzen Public IPs bzw. den Public-Hostname einer Instanz weiss, oder dass man weiss in welcher Cloud man sich befindet.
Seit einiger Zeit lässt sich das ganz einfach mit Jinja-Templates erreichen:

https://cloudinit.readthedocs.io/en/latest/explanation/instancedata.html#using-instance-data

Erzeugt eine VM die im Doc-Root des Apache-Webservers ein index.html hat welches folgendes enthält:
```html
<html>
<body>
My hostname is : <publichostname> <br>
I am running in the cloud: <cloudname> <br>
My distro is: <distroname> <br>
</body>
</html>
```

[Lösung](./01_Auftraege/70_jinja_templates_metadata_loesung.md)


## Cloud-Init: Ansible Master Server aufsetzen

Bei Ansible gibt es nicht allzuviel zu installieren. Es braucht einen Server auf welchem Python installiert ist, da Ansible in Python geschrieben ist und es braucht noch die Installation von Ansible selbst. 

Dazu gibt es mehrere Möglichkeiten.
Ich habe die soweit als möglich OS-unäbhängige Version in [Installation von Ansible](../E_Ansible/README.md#installation) beschrieben.

Danach sollte Ansible installiert sein.

Jetzt fehlt nur noch euer geheimes privates Git-Repo in welchem ihr alle eure Playbooks verwaltet. Dieses solltet ihr auch noch klonen. Dazu könnt ihr die Skripte vom Auftrag [Cloudinit-Zerotrust](#cloud-init-und-zero-trust) benutzen.

**Kurz zusammengefasst:** Benutzt die Lösung zum Auftrag [Cloudinit-Zerotrust](#cloud-init-und-zero-trust) um Ansible zu installieren und noch ein oder zwei "geheime" Ansible-Repos zu klonen.

[Meine Lösung findet ihr hier im *ansible_install*-Branch](https://gitlab.com/armindoerzbachtbz/cloudinit_zerotrust/-/tree/ansible_install?ref_type=heads)

### Lernziele / Taxonomie

- Ihr kennt die Standard-Module von Cloud-Init
- Ihr könnt diese in einer einfachen Version anwenden
- Ihr könnt ein Ansible installieren

