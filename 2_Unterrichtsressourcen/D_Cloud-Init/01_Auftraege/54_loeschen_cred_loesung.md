```bash
#!/bin/bash

ACCESSTOKEN=$1

if [ -z $ACCESSTOKEN ] ; then
  echo "no accesstoken"
  exit 1
fi

# einlesen der ID des keys zum Loeschen, welche beim hinzufuegen per API zurueckgegeben wurde. 
ID=$(cat /tmp/sshkey-id)

# Warten bis cloud-init disabled ist.  Achtung die zwei // braucht es bei Windows!!
until multipass exec securevm -- test -f //etc/cloud/cloud-init.disabled ; do
   echo waiting
   sleep 10
done
  

# Rest-API call zum loeschen des Pulbic Keys.
curl -X DELETE --header "PRIVATE-TOKEN: $ACCESSTOKEN" "https://gitlab.com/api/v4/user/keys/$ID" 
```

