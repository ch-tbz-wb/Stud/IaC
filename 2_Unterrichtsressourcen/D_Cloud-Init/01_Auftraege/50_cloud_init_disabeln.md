# Cloud-Init disabeln

Im ersten Schritt geht es darum, dass Cloud-Init nach der Installation disabled werden soll, d.h. bei einem erneuten reboot der Machine soll cloud-init nicht laufen, damit der Cloud-Provider bei einem Reboot keine Änderungen mehr durchführen kann.

Erstellt eine die User-Data für Cloud-Init, die zum Schluss Cloud-Init disabled. 

Dazu könnt ihr im der [Theorie](../02_Theorie/10_cloudinit_ablauf_beschreibung.md) nachlesen, wie entschieden wird ob Cloud-Init starten soll oder nicht.


[Lösung](./50_cloud_init_disabeln_loesung.md)
