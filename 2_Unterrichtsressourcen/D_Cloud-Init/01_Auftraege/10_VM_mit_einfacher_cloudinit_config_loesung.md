# Lösung: Erstellen einer VM mit einer einfachen Cloud-Init konfiguration 

```
multipass launch -n firststepsvm --cloud-init 10_VM_mit_einfacher_cloudinit_config_loesung.yml
```

[10_VM_mit_einfacher_cloudinit_config_loesung.yml](./Resourcen/10_VM_mit_einfacher_cloudinit_config_loesung.yml) ist die benutzte Cloud-Init-Datei.

