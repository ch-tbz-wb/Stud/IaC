# Auftrag Cloud-Init-Validation


# Validieren eines guten cloud-init Files
Als erstes sollte ihr eine laufende `cloud-init`-Datei validieren. Dazu benutzt ihr den Guide [Validate my user data](https://cloudinit.readthedocs.io/en/latest/howto/debug_user_data.html).

- Nehmt das `cloud-init.yml`, das ihr letzten Auftrag Wordpress erzeugt habt.
- Kopiert das auf eine laufende Linux-VM
- benutzt `cloud-init schema ...` um es zu validieren. (Was kommen nomalerweise für meldungen?)

# Validieren eines kaputten cloud-init Files

- Validiert folgenendes [cloud-init.yml](./31_cloudinitvalidation/cloud-init.yml)-File.
- Was ist hier falsch?

