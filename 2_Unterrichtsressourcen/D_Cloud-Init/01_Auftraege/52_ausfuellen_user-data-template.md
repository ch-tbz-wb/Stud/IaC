# Ausfüllen von User-Data-Template

Jetzt erstellen wir aus einem User-Data Template die User-Data mit den im Auftrag 2 erstellten private SSH-Key.

Hier die Steps:
- Erstellt ein Cloud-Init User-Data-Template file in welchem ihr den Platz, wo der Private SSH-Key hingehört, mit einem TAG (z.B `SSH-KEY-PLACE`) markiert.
- In diesem Template könnt ihr dann ganz einfach mit `git clone` ein privates Repo per ssh clonen, zum eigenen Code zu installieren, ...
- Erstellt ein Skript welches das Template file mit dem richtigen SSH-Key von Auftrag 2 abfüllt. (sed benutzen: `sed -i 's/SSH-KEY-PLACE/'$(cat private.key)'/g'`)

[Lösung](./52_ausfuellen_user-data-template_loesung.md)
