# Aufsetzen von einem Wordpress-Server

Benutze folgende Module um einen apache-php-wordpress-server zu installieren und starten:

- [package-update-upgrade-install](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#package-update-upgrade-install)
- [write-files](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#write-files)
- [runcmd](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#runcmd)

Benutze dazu Shell-Skripte um die Konfiguration von Wordpress und Mysql vorzunehmen. Dazu solltest du zumindest einen Teil der [BASH-Tutorials gemacht haben](../../C_Bash/README.md).


Du musst damit folgende Komponenten installieren:

- [MariaDB](https://wiki.ubuntuusers.de/MariaDB/)
- [PHP & Apache](https://wiki.ubuntuusers.de/PHP/)
- [Wordpress](https://wordpress.org/documentation/article/how-to-install-wordpress/)

Vorgehen:

# Step 1

Google nach "wordpress installation"

# Step 2

Ich versuche diese Anleitung in einer neuen VM von Hand nachzuvollziehen, und notiere mir alle Kommandos die es braucht.

# Step 3

Immer noch von Hand eine neue VM aufsetzen und alles nach den Notizen durchfuehren, solange bis es reibungslos funktioniert.

# Step 4

Cloud-init schreiben und testen.


[Eine moeliche Lösung](./40_apache_php_wordpress_cloudinit_loesung.md)