Cloud-Init User-Data-Template (user-data-template.yml)
```cloud-config
#cloud-config


write_files:
- path: /tmp/gitlabkey
  permissions: '0600'
  content: |
SSH-KEY-PLACE

runcmd:
- export GIT_SSH_COMMAND="ssh -i /tmp/gitlabkey -o StrictHostKeyChecking=no"
- git clone git@gitlab.com:whatever/repo.git /root/repo
- touch /etc/cloud/cloud-init.disabled

```

Skript to populate User-Data:
```bash
#!/bin/bash
# Fuer das yaml-File muessen wir den SSH-Private-Key um 4 spaces einruecken
# Das geschieht mit sed und dem 's/^/    /g' was soviel bedeutet wie
# Fuege am Anfang der Zeile (^) 4 Leerzeichen ein.
sed 's/^/    /g' /tmp/gitlabkey > /tmp/gitlabkey.mod
# Jetzt fuegen wir nach der Zeile mit SSH-KEY-PLACE im user-data-template.yml das
# File /tmp/gitlabkey.mod ein und loeschen mit dem zweiten sed die Zeile SSH-KEY-PLACE
# und speichern den output in /tmp/user-data.yml
sed '/SSH-KEY-PLACE/r /tmp/gitlabkey.mod' $(dirname $0)/user-data-template.yml | sed '/SSH-KEY-PLACE/d'> /tmp/user-data.yml

```