Die Loesung koennte so aussehen:
```yaml
## template: jinja
#cloud-config

packages:
  - apache2
ssh_authorized_keys:
  - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA3YXZTrjfrkxqadOufvEFZznt4ja8pGi6ylpYPu2QATpzOXTY3eX3Z7ql9ZYZ0xyKi+rKsKKNffqE1Wj4rm32iaThb6AOJvANDaIJJeLSDU/bmnQ8fS4RA2PiAqs7Pl+bbuUy431TdRcMNQ2r5eUmDyo1G5a/7mWC0TvW6qQSttlGe+fsLnkqK2xB/IvqoIaNSHmyWSFa8KNNnMqSrH+jZgxTxLCrvXt9bN86DOybe9cLkBEwooL1iTIw1j+7BQtWYCys3vM8DIIENOOuynZs9VHjYuayFjkyxu2vptwZujWqtSLGUBWEcXa49C1iHOMypRsTW5KM7Ndu/IrFGLsRjw== ado@macbookair.local

write_files:
- path: /var/www/html/index.html
  permissions: '0444'
  content: |
    <html>
    <body>
    My hostname is : {{ ds.meta_data.public_hostname }} <br>
    I am running in the cloud: {{ v1.cloud_name }}  <br>
    My distro is: {{ v1.distro }} <br>
    </body>
    </hmtl>
runcmd:
- touch /etc/cloud/cloud-init.disabled
```