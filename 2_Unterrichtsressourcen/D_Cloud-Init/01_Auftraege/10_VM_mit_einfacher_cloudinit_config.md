# Erstellen einer VM mit einer einfachen Cloud-Init konfiguration

Erstellt eine VM mit multipass, in welcher ihr folgende Dinge mit Cloud-Init konfiguriert:

- [Gruppen und User](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#users-and-groups):
  - erzeugt eine Gruppe `students`.
  - erzeugt die User bob und john die `students` als [Primary Group](https://docs.oracle.com/cd/E19253-01/817-1985/userconcept-35906/index.html) haben und als [Secondary Group](https://docs.oracle.com/cd/E19253-01/817-1985/userconcept-35906/index.html) `sudo`
  - Setzt Passwörter für die beiden user mit [chpasswd](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#set-passwords) und erlaubt ssh-login.
