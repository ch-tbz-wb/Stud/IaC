

# Auftrag: Logfiles

- Findet im Logfile heraus, woher cloud-init euer cloud-init script bekommt,
- und wo es dieses hinschreibt (cached).   

Tip: sucht nach `user-data`

# Auftrag: Cache-Directory

Untersucht den Inhalt des Cache-Verzeichnisses. Welche Inhalte findet ihr dort

# Auftrag: Stages

- Versucht im Logfile, die [hier](https://cloudinit.readthedocs.io/en/latest/explanation/boot.html) beschreibenen Bootstages (Local, Network, Config, Final), zu finden, 
- und versucht herauszufinden was in welchen Stages konfiguriert wird. 

# Auftrag: Datasource

Im Logfile solltet ihr eigentlich gefunden haben woher die user-data geholt wurde.
Nur ein paar Zeilen davor seht ihr, welches Device als Datasource gefunden und gemounted wird.

Tip: sucht nach der ersten Zeile im Logfile welches `DataSource` enthält

- Mounted das Iso-Image und schaut euch den Inhalt an. Was findet ihr dort? 

Datasourcen sind ziemlich wichtig, darum sollte man verstehen was darin übermittelt wird.

