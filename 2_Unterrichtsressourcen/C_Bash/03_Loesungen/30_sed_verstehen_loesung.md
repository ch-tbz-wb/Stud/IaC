# Loesungsvorgehen
Nachdem ich 
```
sed 's/conditions/CONDITIONS/g' /usr/share/common-licenses/BSD >BSD
```
ausgeführt habe vergleiche das neue File `BSD` mit dem Inputfile `/usr/share/common-licenses/BSD`. Das kann ich mit einem der folgenden Kommandos tun:
```
diff /usr/share/common-licenses/BSD BSD
sdiff /usr/share/common-licenses/BSD BSD
vimdiff /usr/share/common-licenses/BSD BSD
```
`diff` produziert dann folgenden Output:
```
5c5
< modification, are permitted provided that the following conditions
---
> modification, are permitted provided that the following CONDITIONS
```
In diesem Output sehen wir, dass nur in der Zeile 5 `conditions` durch `CONDITIONS` ersetzt wurde. Alle anderen Vorkommnisse von `conditions` wurden nicht ersetzt.

Dann führe ich das zweite Kommando aus
```
sed -i.backupcopy '/Redistribution and use/,/are met:/ s/CONDITIONS/Conditions/g' BSD
```
und vergleiche das file `BSD` mit dem File `BSD.backupcopy`
```
5c5
< modification, are permitted provided that the following Conditions
---
> modification, are permitted provided that the following CONDITIONS
```
Jetzt wurde im File `BSD` direkt CONDITIONS durch Conditions ersetzt und vom original eine Backupkopie `BSD.backupcopy` erstellt.

Wenn man nun die Kommandos ohne `/Redistribution and use/,/are met:/` benutzt:
```
sed 's/conditions/CONDITIONS/g' /usr/share/common-licenses/BSD >BSD
sed -i.backupcopy 's/CONDITIONS/Conditions/g' BSD
```
Dann sieht man folgende unterschiede:
```
ubuntu@jinja:~$ diff /usr/share/common-licenses/BSD BSD
5c5
< modification, are permitted provided that the following conditions
---
> modification, are permitted provided that the following Conditions
8c8
<    notice, this list of conditions and the following disclaimer.
---
>    notice, this list of Conditions and the following disclaimer.
10c10
<    notice, this list of conditions and the following disclaimer in the
---
>    notice, this list of Conditions and the following disclaimer in the
ubuntu@jinja:~$ diff BSD BSD.backupcopy 
5c5
< modification, are permitted provided that the following Conditions
---
> modification, are permitted provided that the following CONDITIONS
8c8
<    notice, this list of Conditions and the following disclaimer.
---
>    notice, this list of CONDITIONS and the following disclaimer.
10c10
<    notice, this list of Conditions and the following disclaimer in the
---
>    notice, this list of CONDITIONS and the following disclaimer in the
```
Man sieht dass nicht nur einmal `conditions` gefunden und ersetzt wurde sondern 3 mal. Warum?

# Erklärung

Hier die Erklärung von Chatgpt und mir:

Das erste `sed`-Kommando `sed 's/conditions/CONDITIONS/g' /usr/share/common-licenses/BSD >BSD` macht folgendes:

1. `'/Redistribution and use/,/are met:/ s/conditions/CONDITIONS/g'`:
   - Der Ausdruck `/Redistribution and use/,/are met:/` definiert den Bereich in der Inputdatei `/usr/share/common-licenses/BSD` der bearbeitet werden soll. Er beginnt bei der Zeile, die den Text "Redistribution and use" enthält, und endet bei der Zeile, die den Text "are met:" enthält. Den Rest der Datei wird nicht verändert.
   - Innerhalb dieses Bereiches wird der Ausdruck `s/conditions/CONDITIONS/g` angewendet, was bedeutet, dass alle Vorkommen des Wortes "conditions" durch "CONDITIONS" ersetzt werden.

2. Das Kommando modifiziert nicht das Inputfile selbst, sondern gibt den modifizierten Inhalt nur aus.  
3. Mit `>BSD` wird dann dieser Output ins neue File `BSD` umgeleitet.

Das zweite `sed`-Kommando `sed -i.backupcopy '/Redistribution and use/,/are met:/ s/CONDITIONS/Conditions/g' BSD` macht folgendes:
1. `-i.backupcopy` steht für: Modifiziere das File selbst aber mache davor eine Backupkopie mit der Endung `.backupcopy`.
2. `'/Redistribution and use/,/are met:/ s/CONDITIONS/Conditions/g'`:
   - Der Ausdruck `/Redistribution and use/,/are met:/` definiert den Bereich in der Datei `BSD` der bearbeitet werden soll. Er beginnt bei der Zeile, die den Text "Redistribution and use" enthält, und endet bei der Zeile, die den Text "are met:" enthält. Den Rest der Datei wird nicht verändert.
   - Innerhalb dieses Bereiches wird der Ausdruck `s/CONDITIONS/Conditions/g` angewendet, was bedeutet, dass alle Vorkommen des Wortes "conditions" durch "CONDITIONS" ersetzt werden.
  
Zusammengefasst bedeutet die beiden `sed`-Kommando, dass es in der Datei `/usr/share/common-licenses/BSD` nach dem Textbereich zwischen "Redistribution and use" und "are met:" sucht. Innerhalb dieses Bereichs ersetzt es alle Vorkommen des Wortes "conditions" durch "CONDITIONS" und dann "CONDITIONS" durch "Conditions". Das Ergebnis wird dann in eine neue Datei mit dem Namen "BSD" geschrieben und vom Zwischenresultat wird eine Backupkopie `BSD.backupcopy`.