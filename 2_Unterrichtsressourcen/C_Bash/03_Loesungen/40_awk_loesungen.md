### Lösung zu: Gebe alle Usernamen in /etc/passwd aus die eine User-ID >= 1000 haben:
```
awk -F: '$3>=1000 {print $1}' /etc/passwd
```
#### Erklärung
- `-F:`: Setzt das Feldtrennzeichen auf `:` (Doppelpunkt), da die `/etc/passwd`-Datei durch Doppelpunkte getrennte Felder hat.
- `$3>=1000` ist die Bedingungung, dass alles was in der `{   }`-Klammer kommt ausgeführt wird. D.h. für jede Zeile im File wird zuerst die Bedingung geprüft und dann allenfalls die Kommandos in der `{   }`-Klammer ausgeführt.
- `print $1` gibt dann die erste Spalte aus. 

### Lösung zu: Gebe die Anzahl Einträge in /etc/passwd aus, welche die Login-Shell(letzte Spalte) `/usr/sbin/nologin` haben und User-ID <=900:

```
awk -F: '$3<=900 && $NF=="/usr/sbin/nologin" {s+=1} END {print s}' /etc/passwd
```

#### Erklärung 

- `-F:`: Setzt das Feldtrennzeichen auf `:` (Doppelpunkt), da die `/etc/passwd`-Datei durch Doppelpunkte getrennte Felder hat.
- `$3 <= 900`: Überprüft, ob die Benutzer-ID (das dritte Feld) kleiner oder gleich 900 ist.
- `&&`: ist ein logisches UND, d.h. sowohl die Bedingung vor `&&` wie auch dahinter müssen erfüllt sein. Es gäbe auch ein ODER `||`.
- `$NF == "/usr/sbin/nologin"`: Überprüft, ob das letzte Feld `$NF` (das die Login-Shell des Benutzers enthält) gleich `/usr/sbin/nologin` ist.
- `{s+=1}`: Erhöht den Zähler `s` um 1, wenn beide Bedingungen erfüllt sind. `s` ist eine Variable. Jede Variable wird mit dem Wert `0` initialisiert, wenn nichts anderes angegeben wird. D.h. beim ersten mal `s+=1`  wird s=1.
- `END {print s}`: Gibt den Wert des Zählers `s` am Ende der Ausführung aus, d.h. wenn die letzte Zeile des Files analysiert wurde. Das Resultat ist also die Anzahl Zeilen die beide Bedingungen erfüllen.


