#!/bin/bash

function usage(){
	echo "Usage: `basename $0` <username> [<username> ...]" >&2
	exit 1
}

if [ -z $1 ] ; then
	usage
else
	users=$*
fi

#Do a restore for each user

for user in $users ; do
	#List available Backups first
	
	if ls /tmp/${user}_home* >/dev/null 2>&1 ; then
		echo "Available Backups for user $user:"
		ls /tmp/${user}_home* 
		while [ ! -r "$backupfilename" ] ; do
		 	read -p "Enter the Backupfilename you want to restore:" backupfilename
		done
		
		echo "Restoring contents of $backupfilename"
		cd /
		tar xvf $backupfilename
	else
		echo "No Backups for user $user available. Skipping this user"
	fi
done
