# Vorgehen

1. Zuerst schauen was im Original-File so drin ist mit beliebigem Editor. Erkenntnis die Zeile im File `/etc/ssh/sshd_config` ist auskommentiert, d.h. sie zeigt einfach die default Einstellungen von `sshd`:
    ```
    #AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2
    ```
    Wenn wir nun das verhalten von `sshd` ändern wollen muessen wir den `#` vorne und den Ausdruck hinten entfernen `.ssh/authorized_keys2`. 

2. Wir müssen ein `sed`-`s/.../.../`-statement zusammenbasteln:
   
Hier eine Variante mit 2. Schritten:
```
sed -i 's/#AuthorizedKeysFile/AuthorizedKeysFile/' /etc/ssh/sshd_config
sed -i '/^AuthorizedKeysFile/ s/.ssh/authorized_keys2//' /etc/ssh/sshd_config
```
Hier die Variante in einem Schritt:
```
sed -i 's/#AuthorizedKeysFile.*/AuthorizedKeysFile	.ssh/authorized_keys/' /etc/ssh/sshd_config
```

Beide Varianten funktionieren. Die erste wird immer funktionieren unabhängig davon ob im Konfigfile noch ein trittes File (z.b. `.ssh/authorized_keys3`) stehen würde und nur genau das File `.ssh/authorized_keys2` entfernen. Auch spielt es keine rolle ob die Zeile noch auskommentiert war oder nicht.
Bei der zweiten Varianten geht man davon aus, dass die auskommentierte Originalzeile drin war und diese nicht geändert wurde.
