#!/usr/bin/bash
apt-get update
apt-get upgrade -y
apt-get install -y php php-mysql mariadb-server pwgen
wget https://wordpress.org/latest.tar.gz -O /var/tmp/latest.tar.gz
cd /var/tmp/ ;  tar xvfz /var/tmp/latest.tar.gz; mv wordpress/* /var/www/html
rm /var/tmp/latest.tar.gz
mysqlpasswd=$(pwgen 40 1)
mysqluser=$(pwgen -0 10 1)
mysql -u root <<EOF
create database wordpress;
GRANT ALL PRIVILEGES ON wordpress.* TO "$mysqluser"@"localhost" IDENTIFIED BY "$mysqlpasswd";
EOF
echo $mysqlpasswd>/var/tmp/mysqlpasswd
echo $mysqluser>/var/tmp/mysqluser
cd /var/www/html
cp wp-config-sample.php wp-config.php
sed -i -e 's/database_name_here/wordpress/' wp-config.php
sed -i -e "s/username_here/$mysqluser/" wp-config.php
sed -i -e "s/password_here/$mysqlpasswd/" wp-config.php
sed -i -e "/SECURE_AUTH_KEY/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/AUTH_KEY/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/LOGGED_IN_KEY/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/NONCE_KEY/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/SECURE_AUTH_SALT/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/AUTH_SALT/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/LOGGED_IN_SALT/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
sed -i -e "/NONCE_SALT/ s/put your unique phrase here/$(pwgen 50 1)/" wp-config.php
rm /var/www/html/index.html
