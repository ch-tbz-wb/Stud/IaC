# Erstellen einer Logfunktion

In diesem Auftrag sollt ihr eine Version der Logfunktion `log` erstellen.
Diese soll in einem seperaten File `$BINDIR/common_functions.bash` gespeichert sein.

Ihr könnte eine der Versionen auswählen:

## Die einfache Version der Logfunktion soll:

- Den übergebenen Text ausgeben
- Vor dem Text ein Zeitstempel im Format 'YYYY-MM-DD mm:hh:ss:' einfügen wie:
  ```
  2024-12-31 12:23:23: Text
  ```
- Falls die Variable `$LOGFILE` gesetzt ist die ausgabe in das File `$LOGFILE` machen, anstatt auf den
  *Standart Output*.


## Die komplizierte Version der Logfunktion soll zusätzlich:

Falls ihr die einfache Version schon beendet habt, könnt ihr euch noch überlegen, wie man Log-Levels wie 
`DEBUG`,`INFO`,`WARNING` und `ERROR` einführen könnte.  
Man kann bei jeder Log-Ausgabe einen Level mitgeben
und die Ausgabe dann im Format `2024-12-31 12:23:23:WARNING: Es ist eine Fehler bei xyz passiert` ausgeben.  
Falls die Variable `$LOGLEVEL` gleich oder tiefer gesetzt ist als der Log-Level der übergeben wird soll nichts ausgeben werden. 
(`DEBUG` ist der tiefste Log-Level und `ERROR` der höchste, d.h. wenn `$LOGLEVEL` auf `DEBUG` gesetzt wir werden
alle Log-Ausgaben ausgegeben bei `ERROR` nur `ERROR`-Logs und keine anderen.

# Verfügbarmachen der Logfunktion in euren Skripten:

Jetzt wo ihr eine solche Funktion erstellt habt,
wollt ihr die möglichst einfach in allen Skripten verfügbar machen.
Dazu benutzt ihr die Variabeln die ihr in `common_variables.bash` gesetzt habt, und sourced einfach das `common_functions.bash`:

```commandline
source $BINDIR/common_functions.bash
```

Wo könnt ihr diese Zeile einfügen, sodass es ihr die Funktion `log` immer zur Verfügung habt?
