# Relozierbare Skripts erzeugen

Hier sollt ihr 3 Skripts erzeugen, welche einander aufrufen:

- Skript:`useradmin.bash`:
  - Es soll die Benutzeradminstration von Unix-Usern ermöglichen, mit folgenden Funktionen:
    - Erstellen von neuen Benutzern
    - Modifizieren von Benutzerattributen wie Primary Group, User-ID
    - Löschen von Benutzern
  - `useradmin.bash` soll Menügesteuert sein.
  - Die Skripts sollen alle relozierbar sein.
  - Um diese Aufgaben zu erfüllen soll es folgende Subscripte benutzen, welche nicht interaktiv sein sollen:
      - `check_if_user_exists.bash`: Überprüft ob der neue User schon existiert.
      - `create_user.bash`: Erzeugt den Benutzer wie gewünscht.
      - `change_uid.bash`: Ändert die UID vom Benutzer und ändert den Owner(UID) aller Files, des Benutzers.
      - `change_primary_group.bash`: Ändert die Primary Group des Benutzers.
      - `delete_user.bash`: Löscht den Benutzer und wenn gewollt alle Files von welchen er Owner ist.