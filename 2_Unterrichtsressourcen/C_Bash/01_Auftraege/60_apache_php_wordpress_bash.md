# Aufsetzen eines WordPress-Servers

Vorgehen:

## Schritt 1

Google nach "WordPress-Installation".

## Schritt 2

Ich versuche, diese Anleitung in einer neuen VM manuell nachzuvollziehen und notiere dir alle Befehle, die benötigt werden.

## Schritt 3

Erneut manuell eine neue VM aufsetzen und alles gemäss den Notizen durchführen, bis es reibungslos funktioniert.

## Schritt 4

Schreibe ein Bash-Skript und teste es.

## Schritt 5

Setze eine VM auf in AWS-Learner-Lab und gebe das Bash-Skript als User-Data mit.

## Lösung
[Eine mögliche Lösung](../03_Loesungen/wordpress/)