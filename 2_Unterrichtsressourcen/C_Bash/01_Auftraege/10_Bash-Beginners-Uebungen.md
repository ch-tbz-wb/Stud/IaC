# Übungen für Beginners

[[_TOC_]]
# Commandline Parameter einlesen

Ihr schreibt ein Skript das die 2 Parameter entgegen nimmt und diese ausgibt mit dem Text 'Das ist Parameter x:'

# Output umleiten

Wenn auf der Bash ein Kommando ausgeführt wird, geben viele Kommandos eine Ausgabe zurück. Diese kann in ein File umgeleitet werden. Das geschieht mit `>` (File neu erstellen mit output) und `>>` (output an bestehndes File anhängen).

Macht ein Skript das den Output von folgenden Kommandos in ein File mit dem Namen `/var/tmp/output.log` umleitet, sodass der output von allen Kommandos im File `/var/tmp/output.log` ist:

```
echo 'Das ist Parameter 1:'
echo $1
echo "Das ist Paraemeter 2: $2"
uname -a
echo 'Das war der output von uname -a'
date
echo 'Das war der output von date'
```

# Variablen definieren

Da es mühsam ist jedes mal den Filenamen `/var/tmp/output.log` anzugeben, führen wir eine Variable ein. Diese soll `LOGFILE` heissen. Diese benutzen wir dann, wenn wir in das File schreiben wollen. Ändert das Skript, dass nur noch genau einmal `/var/tmp/output.log` im Skript vorkommt:

```
LOGFILE=....


echo .... >> $LOGFILE
```

# Logs schreiben

Skripte sollen logs schreiben. Dazu wird in bash oft einfach der output von Kommandos an ein Logfile angehängt, bzw. Kommmantare mit echo ausgegeben.
Gut waere es wenn dies immer in einem einheitlichen Format mit Timestamp geschieht, wie z.B: mit:

```
echo "$(date +'%Y%m%d %H:%M:%S'): was auch immer" >> output.log
```
Ergaenzt die Kommandos im Skript von mit einem Timestamp for jeder Zeile die output generiert.

Ihr könnt dabei immer mit `echo  "$(date +'%Y%m%d %H:%M:%S'): euer text"` oder `echo "$(date +'%Y%m%d %H:%M:%S'): $(euer kommando)"` benutzen.

# Funktionen definieren
 
Jetzt haben wir x Zeilen mit `echo "$(date +'%Y%m%d %H:%M:%S'):..." >>$LOGFILE`. Kann man das nicht besser machen?
Definiert eine Funktion log die das für euch tut, sodass ihr nur noch folgendes habt:
```
LOGFILE=/var/tmp/output.log
log(){
    #hier kommt euer Code
}
log "Das ist Parameter 1:"
log "$1"
log "Das ist Parameter 2: $2"
log $(uname -a)
log 'Das war der output von uname -a'
log $(date)
log 'Das war der output von date'
```

# while-Loop

Jetzt möchte ich dass alle Paraemter die übergeben werden, ausgegeben werden und nicht nur die ersten beiden.

Dazu benutzen wir den while loop und ersetzen die Zeilen

```
log "Das ist Parameter 1:"
log "$1"
log "Das ist Parameter 2: $2"
```
durch einen Loop:
```
count=.....
while [ $# -gt 0 ] 
do
  # Logge den Parameter
  # shifte die Parameter (Tip: es gibt ein bash kommando fuer das)
  # Erhoehe count um 1
done

```

# Testen von File-Eigenschaften

Jetzt sollt ihr für jeden input Paremeter schauen ob 
- ein File existiert
- es lesbar ist
- es ausführbar ist
- es ein Directory ist
- ob die Filegrösse 0 ist, d.h. es ein leeres File ist.

und das loggen.

Dazu benutzt ihr den Loop von vorher und und testet den Paramter mit 
```
if [ -TEST $1 ] 
then
   log "File ist ..."
fi
```
Wobei **-TEST** eines von `-x`,`-r`,`-d`,`-e`,`-s` ist (Tip:  `man test` hilft)

# while-Loop durch for-Loop ersetzen

Da wir als Übergabeparameter Filenamen erwarten und für jeden von diesen etwas tun sollen, können wir statt dem `while`-Loop auch einen `for`-Loop benutzen.

Ersetze `while` durch `for` sodass genau das Gleiche ausgeben wird. (Tip: `shift` braucht es nicht mehr)

# Ausführen von anderen Skripts oder Kommandos

Jetzt geht darum, dass ihr alle Files die ihr vorher getestet habt und welche ausführbar waren auch ausführt.

Tut dies indem ihr einfach etwas wie

```
$filename
``` 
am richtigten Ort einfügt. (ich gehe davon aus, dass ihr einen for-Loop wie `for filename in $*` benutzt habt)

# Exit-Code von aufgerufen Skripts oder Kommandos überprüfen und `stderr`-Output ausgeben.

Jetzt sollt ihr den Exit-Code überprüfen. Falls es einen Fehler gegeben hat, sollt ihr den `stderr`-Output  ausgeben sonst nicht. Leitet dazu den `stderr`-Output in ein temporäres File (Tip: `2>$TMPFILE` benutzen) um und gebt dieses aus, falls der Exit-Code einen Fehler anzeigt (Tip: Exit-Code ist ungleich 0 bedeutet es gab ein Fehler, Tip 2: `cat $TMPFILE >&2` benutzen).
