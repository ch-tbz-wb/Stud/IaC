# Wie bediene ich sed in der bash und wie verwende ich REGEX?

[TOC]

## Einleitung
<p>Da die man-page von sed schlecht dokumentiert ist und wenig Hilfestellung gibt wie der Befehl funktioniert,<br>
habe ich mich "freiwillig" dazu entschlossen meinen Mitschülern eine kleines übersichtliches Tutorial zu schreiben.</p>

## sed syntax

Es beginnt schon mal hoffnungslos mit der Erklärung der Syntax. Diese ist ersichtlich wenn man die man Page vom sed befehl anschaut.

```bash
sed [options]…  [script-only-if-no-other-script] [input-file]...
```

<p>Wir haben das Kommando sed und die Optionen welche weiter unten beschrieben sind. Bis jetzt verständlich.<br>
Was soll [script-only-if-no-other-script] überhaupt bedeuten?<br>
Wenn wir die Option -f anschauen steht dort, script-file. Wir können den Inhalt eines Skript files dem Kommando sed übergeben.<br>
Dies ist aber bereits schon zu kompliziert.<br>
<br>
In diesem Tutorial werden wir einfache Beispiele verwenden und bestehende Files anpassen.
</p>

Zudem habt ihr hier noch eine Tabelle mit den gängigsten Zeichen. Meistens setzt man ein \ vor den Zeichen die man verwenden möchte.

| Syntax            | Beschreibung                                                  |
|-------------------|--------------------------------------------------------------|
| `.`               | Passt auf ein einzelnes Zeichen außer einer neuen Zeile (`\n`).          |
| `^`               | Passt auf den Anfang einer Zeile.                                 |
| `$`               | Passt auf das Ende einer Zeile.                                   |
| `*`               | Passt auf null oder mehr Vorkommen des vorherigen Zeichens.  |
| `+`               | Passt auf ein oder mehr Vorkommen des vorherigen Zeichens.  |
| `?`               | Passt auf null oder ein Vorkommen des vorherigen Zeichens.   |
| `[]`              | Passt auf ein einzelnes Zeichen innerhalb der Klammern.            |
| `[^]`             | Passt auf ein einzelnes Zeichen, das nicht innerhalb der Klammern steht.        |
| `\`               | Entkommt Sonderzeichen oder führt spezielle Sequenzen ein.  |
| `()`              | Gruppiert und erfasst übereinstimmende Teilausdrücke.                  |
| `Pipe`            | Alternation, passt entweder auf den Ausdruck vor oder nach dem Trennzeichen.  |
| `{n}`             | Passt genau `n` Vorkommen des vorherigen Zeichens.  |
| `{n,}`            | Passt auf `n` oder mehr Vorkommen des vorherigen Zeichens. |
| `{n,m}`           | Passt auf mindestens `n` und höchstens `m` Vorkommen.           |
| `\d`              | Passt auf jedes Ziffernzeichen (entspricht `[0-9]`).         |
| `\D`              | Passt auf jedes Nicht-Ziffernzeichen (entspricht `[^0-9]`).    |
| `\w`              | Passt auf jedes Wortzeichen (alphanumerisch und Unterstrich).    |
| `\W`              | Passt auf jedes Nicht-Wortzeichen.                              |
| `\s`              | Passt auf jedes Leerzeichenzeichen.                            |
| `\S`              | Passt auf jedes Nicht-Leerzeichenzeichen.                        |


## Positionierung, Navigation
<p>Damit wir uns zurecht finden lernen wir zuerst wie wir mit sed navigieren können.<br>
Hier habe ich mit ChatGPT ein [Random-Textfile](https://gitlab.com/nedzad.fazlic/sed-101/-/blob/main/exampleFiles/random.txt?ref_type=heads) erstellt.<br>
Wir arbeiten mit der Angabe von der man page das /regexp/ uns zu diesen Zeilen springen lässt.<br>
Nehmen wir den ersten Satz des random.txt Files.<br>
"The cat chased the mouse around the room."</p>

Schauen wir uns ein Beispiel an:

```bash
sed '/The cat chased the/,/around the room/ s/mous/human/' random.txt

# Output
The cat chased the humane around the room...
...
```

<p> Wir möchten das Wort mouse mit human ersetzten. Um es noch besser zu veranschaulichen nehmen wir nur die ersten 4 buchstaben von mouse (mous)<br>
Heisst das wort mouse soll nacher so aussehen: humane 
Die ganze expression wird in Single-Quotes eingesetzt. ''<br>

Mit der ersten Expression teile ich sed mit wo die Zeile beginnt<br>
Dies wird so geschrieben '/The cat chased the/<br>

Mit der zweiten expression ,/around the room/ definiere ich bis wohin sed suchen soll.<br>
Heisst die regularexpression such das Wort zwischen '/The cat chased the/,/around the room/<br>
wobei das Komma den Platz zwischen den beiden Expressions meint<br>

Gratulire, du hast es geschafft, sed mitzuteilen wo der Befehl suchen soll<br>

Nun können wir angeben was wir ersetzten möchten<br>
dies machen wir, indem wir folgende eingabe machen<br>
s/ bedeutet "substitute"<br>
s/altertext/neuertext in unserem beispiel wäre das s/mous/human<br>

Zum schluss geben wir das File an welches editiert werden soll.<br>

### Aufgabe 1, das erste "The"
Im random.txt File beginnen viele sätze mit "The".
Versuchen wir den Befehl mit nur einer Epxression.

```bash
sed '/The/ s/mous/human/' random.txt

# Output
The cat chased the humane around the room...
...
```
<P>Der Output ist derselbe. Warum ist das so?<br>
Mit der Expression wird der Satz ausgewählt wenn dieser mit "The" beginnt. und das Wort 'mous' enthält<br>
Was ist wenn wir nun zwei gleiche Zeilen haben die der Expression entsprechend<br>
Kopiere im random.txt den Ersten Satz gleich unterhalb und verwende den obigen sed befehl im Beispiel<br>
Was kommt dabei raus?</p>

```bash
sed '/The/ s/mous/human/' random.txt 
# Output
The cat chased the humane around the room.
The cat chased the humane around the room.
```
<p>Wie kriegen wir es hin das nur der erste Satz abgeändert wird und der zweite Satz gleich bleibt?<br>
Zusätzlich soll nur die selektion von regex ausgeben werden. Dies erreichen wir wenn wir am schluss der Expression -n /mouse/p eingeben.<br>
-n unterdrückt die Ausgabe des gesamten Files und /p gibt das matching pattern aus.
Wir lassen das substitute mal weg und ersetzen noch nichts
</p>

```bash
sed -n '/mouse/p' random.txt
# Output
The cat chased the humane around the room.
The cat chased the humane around the room.
```
Der obige befehl gibt beide Zeilen aus. Mit einer kleinen Anpassung brechen wir den Filestream nach dem ersten Match ab.
Jawohl /mouse/  ist bereits ein REGEX Pattern und dieses mal geben wir keine weiteren Patterns an. Der Grund dafür ist einfach. Das Wort "mouse" kommt nur einmal vor.

nach /p kann ich mit ;q eine weitere Option anhängen um nach dem erste Match zu quitieren.
```bash
sed -n '/mouse/p;q' random.txt

# Output
The cat chased the humane around the room.
```
### Aufgabe 2,  "The sound"

Setzen wir folgenden Befehl:
```bash
grep "The sound" random.txt
```
Erhalten wir folgenden Output

```bash
# Output
The sound of laughter echoed through the park as children played.
The sound of waves lapping against the boat lulled him to sleep.
The sound of a distant train whistle echoed through the valley.
The sound of church bells rang out, announcing the start of the ceremony.
The sound of footsteps echoed in the hallway, growing louder with each passing moment.
The sound of a baby's laughter filled the room, bringing a smile to everyone's face.
The sound of a distant thunderstorm grew louder, signaling an approaching storm.
The sound of waves crashing against the rocks echoed in the cave.
The sound of children playing in the distance brought back memories of his own childhood.
The sound of a violin drifted through the air, haunting and beautiful.
The sound of a distant train whistle brought a sense of nostalgia.
The sound of a distant thunderstorm rumbled in the distance, sending shivers down her spine.
The sound of children playing echoed through the park, filling the air with joy.
The sound of a violin drifted through the air, haunting and beautiful.
The sound of a distant train whistle brought a sense of nostalgia.
The sound of a distant thunderstorm rumbled in the distance, sending shivers down her spine.
The sound of children playing echoed through the park, filling the air with joy.
```

Wenn wir nur das wort "sound" grep-en dann erhalten wir folgenden output:

```bash
The sound of laughter echoed through the park as children played.
The sound of waves lapping against the boat lulled him to sleep.
The sound of a distant train whistle echoed through the valley.
Leaves rustled in the breeze, creating a symphony of sound.
The sound of church bells rang out, announcing the start of the ceremony.
The sound of footsteps echoed in the hallway, growing louder with each passing moment.
The sound of a baby's laughter filled the room, bringing a smile to everyone's face.
The sound of a distant thunderstorm grew louder, signaling an approaching storm.
The sound of waves crashing against the rocks echoed in the cave.
The sound of children playing in the distance brought back memories of his own childhood.
The crackling of the fire was the only sound in the quiet cabin.
The sound of a violin drifted through the air, haunting and beautiful.
He closed his eyes and listened to the sound of the waves crashing against the shore.
The sound of a distant train whistle brought a sense of nostalgia.
Birds chirped in the trees, their song a symphony of sound.
The sound of a distant thunderstorm rumbled in the distance, sending shivers down her spine.
The sound of children playing echoed through the park, filling the air with joy.
The crackling of the fire was the only sound in the quiet cabin.
The sound of a violin drifted through the air, haunting and beautiful.
He closed his eyes and listened to the sound of the waves crashing against the shore.
The sound of a distant train whistle brought a sense of nostalgia.
Birds chirped in the trees, their song a symphony of sound.
The sound of a distant thunderstorm rumbled in the distance, sending shivers down her spine.
The sound of children playing echoed through the park, filling the air with joy.
The crackling of the fire was the only sound in the quiet cabin.
```
<p>Dies sind einige mehr als im obigen Beispiel.<br>
Wir möchten lediglich die Sätze abändern die mit "The sound" beginnen.<br>
Ersetzen wir das Wort mit "song" mit "cheese"</p>

Folgender Befehl funktioniert nicht, da das Wort "song" in anderen Sätzen die nicht mit "The sound" auch abgändert wird.
Heisst die Angabe Der Position /The/,/of/ wird nicht angenommen.

```bash
sed -n '/The/,/of/ s/sound/cheese/p' random.txt
```
Mit diesem Befehl funktioniert es natürlich da ich ein substitute von "The sound of" mit "The cheese of" ersetzte.

```bash
sed -n 's/The sound of/The cheese of/p' random.txt 
```
Gibt es eine andere Möglichkeit wie ich den sed Befehl ausführen kann um das gleiche Ergebnis zu erhalten?

```bash
sed -n '/^The sound/s/sound/cheese/p' random.txt 
```
Mit dem /^The sound/ spezifizieren wir das nur Sätze selektiert werden die mit "The sound" beginnen.

### Aufgabe 3, Doppelte Einträge

Kann man mit sed doppelte Einträge ausfiltern?
Nein, sed arbeitet auf einer Pro-Linie Basis, heisst wir müssen das file vorher ausfiltern.
Hier wieder ein einfaches Beispiel mit grep um zu sehen welche Sätze mehr als einmal vorkommen.
```bash
grep color random.txt 
A rainbow stretched across the sky, its vibrant colors a beautiful sight to behold.
She smiled as she watched the sunset, the sky ablaze with color.
He watched the sunset, the sky ablaze with color.
She smiled as she watched the sunset, the sky ablaze with color.
He watched the sunset, the sky ablaze with color.
She smiled as she watched the sunset, the sky ablaze with color.
```

<p>Beispiel mit awk<br>
Wir | den awk befehl nach dem sed befehl
awk '!seen[$0]++'<br>
Testet zuerst den sed befehl um zu sehen ob ihr den gleichen Output habt, wie mit dem grep Befehl.</p>

``` bash
sed -n '/color/p' random.txt | awk '!seen[$0]++'
```

Erklärung des awk-befehls:

- ! ist der logische NOT-Operator.
- seen ist ein array, eine Liste die elemente speichert, in unserem Fall werden es Sätze sein die mehrfach vorkommen.
- [$0] repräsentiert die letzte verarbeitete zeile, hiermit erstellen wir einen Eintrag.
- ++ erhöht den Wert des Array Elements um die nächste Linie in den neuen Wert zu speichern.

Zusammenfassend entfernt !seen[$0]++ effektiv Duplikate, indem es nur das erste Auftreten jeder einzigartigen Zeile druckt, während die Reihenfolge der Zeilen in der Eingabe beibehalten wird.

## Globale Änderungen

Mit dem /g operator können wir im ganzen Text alle passenden Suchergebnisse modifizieren.

### Aufgabe 4, "noise"

Ersetzten wir das Wort "sound" mit dem Wort "noise" überall im Text.

```bash
sed -n 's/sound/noise/gp' random.txt
```

<p>Benötigen wir den /g Operator?<br>
Ja natürlich. Denn ohne den /g Operator wird der erste Match in jeder Linie geändert.<br> 
Wenn das Wort "sound" in einem Satz zwei mal vorkommt, wir ohne den /g Operator nur das erste Wort geändert.<br>
Diesen Operator nur verwenden wenn es ok ist, dass alle Wörter geändert werden sollen</p>


### Aufgabe 5, ! statt ,
 Nun als nächste Aufgaben möchtne wir die Kommas mit einem Ausrufezeichen ersetzten.
 Wie bewerkstelligen wir das?

 ```bash
 sed -n 's/,/!/gp' random.txt
 ```

### Aufgabe 6, erkläre den sed Befehl

Was macht folgender Befehl?

```bash
sed -n '/[^a-z]/s///gp' random.txt
```

- -n: Ünterdrückt die automatische Ausgabe von Zeilen die nicht matchen
- '[^a-z]': Dies ist die erste Regular Expression und entspricht jedem Zeichen, das ***kein*** Kleinbuchstabe ist.
- 's///' Dies ist der Substitution-Befehl den wir bereits kennen, jedoch sind die beiden Angaben leer.
- /gp: g steht für global, also wird es überall geändert und p steht für die Ausgabe

<p>Also sucht der Befehl jede Zeile in der Datei random.txt nach einem Zeichen, das kein Kleinbuchstabe ist.<br> 
Wenn es gefunden wird, führt er eine globale Substitution durch (was im Wesentlichen die Nicht-Kleinbuchstaben-Zeichen entfernt)<br> 
und druckt die modifizierte Zeile. Da jedoch das Ersetzungsmuster und das Ersatzmuster fehlen (s///), werden effektiv alle<br>
Nicht-Kleinbuchstaben-Zeichen aus jeder übereinstimmenden Zeile gelöscht und die modifizierten Zeilen gedruckt.</p>

Was macht nun diese Befehl?

```bash
sed -n '/[a-z]/s///gp' random.txt
```

Es ersetzt alle Kleinbuchstaben mit leeren Zeichen. Heisst nur noch Grossbuchstaben, Kommas und Punkte sind ersichtlich.

### Aufgabe 7, backupfile

Die Änderungen solle nun im random.txt File effektiv umgesetzt werden und dazu soll ein backup file erstellt werden.
Nehmen wir den Befehl von vorhin.

```bash
sed -n '/[a-z]/s///gp' random.txt
```

Mit der Option -i wird das File angepasst und setzten wir eine Endung nach der Option wird zu gleich ein Backupfile erstellt.

```bash
sed -n -i.original '/[a-z]/s///gp' random.txt
```

## Advanced sed

Im File test.conf treffen wir auf Verzeichnise, IP Adressen und Konfigurationsparameter.
Versuche die Aufgaben hier zu lösen, ohne dabei weiter unten auf die Lösungen zu schauen.

- Extrahiere nur die Verzeichnispfade.
- Ersetze alle Vorkommen von IP-Adressen durch Platzhalter.
- Entferne alle Kommentare aus der Datei.
- Extrahiere nur die Konfigurationseinträge (Zeilen mit Schlüssel-Wert-Paaren).
- Ändere die Konfigurationseinträge, z. B. Ändern von Werten oder Hinzufügen neuer Einträge.
- Extrahiere nur die IP-Adressen.
- Ersetze alle Vorkommen von Verzeichnispfaden durch Platzhalter.

Suche nach einer Erkärung für die Befehle und erzänge dieses Tutorial für dich selber.


### Advanced: Verzeichnise suchen
Dieser Befehl sucht nach Zeilen in der Datei test.conf, die mit einem Schrägstrich (/) beginnen und beliebige andere Zeichen enthalten. Das /p am Ende gibt alle Zeilen aus, die dem Muster entsprechen. ^ steht für den Anfang der Zeile und $ für das Ende.

```bash
sed -n '/^\/.*$/p' test.conf
```

### Advanced: IP-Adressen maskieren
Dieser Befehl sucht nach IP-Adressen im Format xxx.xxx.xxx.xxx in der Datei test.conf und ersetzt sie durch <IP-Adresse>.   
- [0-9]\+: Dies entspricht einer oder mehreren Ziffern (0-9).
- \(\.[0-9]\+\): Dieser Teil ist in \( und \) eingeschlossen, was bedeutet, dass es sich um eine Gruppe handelt. Es entspricht einem Punkt gefolgt von einer oder mehreren Ziffern.
- \{3\}: Dies entspricht genau drei Vorkommen der vorherigen Gruppe (Punkt gefolgt von Ziffern), um sicherzustellen, dass wir eine IP-Adresse finden.
- /: Dies trennt das Muster, das gesucht werden soll, von dem Ersetzungsmuster.

```bash
sed 's/[0-9]\+\(\.[0-9]\+\)\{3\}/<IP-Adresse>/g' test.conf
```

### Advanced: Kommentare entfernen
Hier werden alle Zeilen, die mit # beginnen (also Kommentarzeilen), aus der Datei test.conf entfernt.

```bash
sed '/^#/d' test.conf
```

### Advanced: Paremeter suchen
Dieser Befehl sucht nach Zeilen in test.conf, die mit einem alphanumerischen Schlüssel beginnen (gefolgt von einem Gleichheitszeichen =) und gibt sie aus.

```bash
sed -n '/^[a-zA-Z0-9]\+=/p' test.conf
```

### Advanced: Parameter anpassen
In der Datei test.conf wird der Wert des Parameters Timeout von 300 auf 600 geändert.

```bash
sed -i 's/Timeout=300/Timeout=600/' test.conf
```

### Advanced: Nach IP-Adressen suchen
Dieser Befehl sucht nach Zeichenfolgen in test.conf, die das Format einer IP-Adresse haben, und gibt sie aus.

- .*: Dies entspricht null oder mehr beliebigen Zeichen am Anfang der Zeile.
- \([0-9]\+\(\.[0-9]\+\)\{3\}\): Dies ist ein Muster für eine IP-Adresse. Es sucht nach einer Sequenz von vier Zahlenblöcken, die durch Punkte getrennt sind. 
- Die Zeichenfolge ([0-9]\+\(\.[0-9]\+\)\{3\}) gruppiert die IP-Adresse.
- \1: Dies ist eine Rückverweis auf das, was innerhalb der Klammern () im Suchmuster gefunden wurde, also die IP-Adresse.

```bash
sed -n 's/.*\([0-9]\+\(\.[0-9]\+\)\{3\}\).*/\1/p' test.conf
```
### Advanced: Verzeichnisse maskieren
Hier werden Verzeichnispfade in test.conf durch <Verzeichnis> ersetzt.

```bash
sed 's/\/[a-zA-Z0-9]\+/\/<Verzeichnis>/g' test.conf
```


## Weitere Aufgaben die mit "sed" möglich sind. Gemäss ChatGPT

Versuche die Folgenden Aufgaben im practise.txt File umzusetzten:

- URLs extrahieren: Extrahiere alle URLs aus einer Datei, um eine Liste von Links zu erhalten.
- HTML-Tags entfernen: Entferne alle HTML-Tags aus einer HTML-Datei, um nur den reinen Text zu behalten.
- Zeilennummer hinzufügen: Füge jeder Zeile einer Datei eine fortlaufende Nummer hinzu.
- Kommentare entfernen: Entferne alle Kommentare aus einer Konfigurationsdatei (zum Beispiel Zeilen, die mit # beginnen).
- Wörter zählen: Zähle die Anzahl der Wörter in einer Textdatei.
- Zeichenketten umkehren: Kehre den Inhalt jeder Zeile um, z.B. von "abc" zu "cba".
- Zeilen umsortieren: Sortiere die Zeilen einer Datei alphabetisch oder nach Länge.

