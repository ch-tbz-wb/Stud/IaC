## Definition Wildcards
 
Wildcards sind spezielle Zeichen oder Zeichenfolgen, die in Suchmustern verwendet werden, um flexiblere Suchkriterien festzulegen. Sie ermöglichen es, Teile eines Musters zu ersetzen oder auf beliebige Zeichen zuzugreifen. Wildcards werden häufig beim Durchsuchen von Texten, Dateien und Verzeichnissen sowie beim Schreiben von Skripten oder Programmen verwendet.
In Bash gibt es Wildcards um Files und Directories zu "suchen". Diese sind hier beschrieben. Sie sind komplett verschieden von Wildcards in [regulären Ausdrücken](regular_expression.md).
 
| Wildcard | Description | Example Usage |
| ---- | ---- |
| `*` (asterisk) | Matches any sequence of characters (including none). | `ls *.txt` |
| `?` (question mark) | Matches any single character. | `ls file?.txt`|
| `[ ]` | Matches any character within the brackets. | `ls file[123].txt` |
| `[ - ]` | Matches any character within the specified range. | `ls file[1-3].txt` |
| `{ }` | Matches any of the comma-separated patterns inside. | `mv file{1,2}.txt directory/` |

Danke an Laura