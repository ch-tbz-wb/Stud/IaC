# Ansible Command-Line Optionen und Environment-Variablen

Es gibt einige Optionen und Environment-Variablen beim Aufruf von Kommandos die immer benutzt werden können. Hier die nützlichsten

## Hostkey check disablen für den ersten Run
```
export ANSIBLE_HOST_KEY_CHECKING=False
ansible -m ping -i inventory.ini hostgroup
unset ANSIBLE_HOST_KEY_CHECKING
```
## Private key file angeben

Private key ist in file `private_key`
```
ansible --key-file private_key -i inventory.ini -m ping hostgroup
```

## User zum einloggen auf der VM angeben

```
ansible -u ubuntu --key-file private_key ....
```

## Als `root`-User laufen lassen
Damit die Tasks gewisse sachen machen können müssen sie `root`-Rechte haben.
```
ansible -u ununtu -b ......
```
