# Cheatsheet zu Bash
[[_TOC_]]

## Parameter Übergabe

Parameter werden an Shell-Skripts einfach als Strings die durch Leerzeichen getrennt sind übergeben. z.B.

```bash
./skript1.bash Doerzbach Armin 31.12.1970
```

Im Skript `skript1.bash` kann dann auf die Parameter zugegriffen werden.
Das geschieht mit den Variablen `$1`,`$2`,`$3`,...

Im `skript1.bash` steht folgendes:
```bash
ls -l $2
```
Dann wird aus `ls -l $2` dann `ls -l Armin`, weil Armin der 2. Parameter ist. Falls das File `Armin` existiert wird es aufgelistet

`-rw-r--r--  1 ado  staff  0 May  9 19:37 Armin` 

und sonst wird ein Fehler ausgegeben

`ls: Armin: No such file or directory`

Falls man wissen will wieviele Parameter übergeben wurden benutzt man `$#`:

```bash
echo "Es wurden $# Parameter übergeben"
```
Produziert dann die Ausgabe:
```
Es wurden 3 Parameter übergeben
```

Falls man alle Parameter hintereinander benutzen will benutzt man `$*` oder `$@`.
```bash
echo "Die Parameter sind: $*"
```
produziert dann den output
```
Die Parameter sind: Doerzbach Armin 31.12.1970
```

Wenn man im skript das Kommando `shift` benutzt werden die Parameter geschiftet. 
D.h. nach dem ersten mal ausführen von `shift` gibt es nur noch `$1` mit `Armin` und $2 mit `21.12.1970` und der Parameter `Doerzbach` wurde gelöscht.

Bei der Übergabe von Parametern an eine Funktion werden innerhalb dieselben `$1`,... benutzt um auf Übergabe-Parameter zuzugreifen. Man kann den übergebenen Parameter keine *Namen* geben wie dies in Java, Python,... üblich ist.

## Parsen von Optionen

Ein Skript soll wie folgt aufgerufen werden

```
delete_users_of_group.bash -g <groupname> -d -f
```

Wobei `-g groupname` der Gruppenname uebergeben werden soll `-d` für lösche Homedirectories `-f` für force, auch wenn der user noch eingeloggt ist.

Um diese Parameter so zu parsen kann man folgendes Code Snipped verwenden
```bash
deletedirectory=0
force=0
while getopts dfg: optvar ; do
    case $optvar in
      d) deletedirectory=1 ;;
      f) force=1 ;;
      g) groupname=$OPTARG ;;
      *) echo wrong parameter ; exit 1 ;;
    esac
done
```

Dann ist deletedirectory bzw. force nur 1 wenn -d bzw. -f mitgeben wurde
und groupname ist gesetzt durch die Option `-g ....`

Das `dfg:` hinter `getopts` definiert, dass die Optionen `-d`,`-f` und `-g` erwartet werden. Der `:` hinter dem `g` bedeutet, dass bei `-g` noch ein Parameter (hier groupname) erwartet wird.

Achtung `$OPTARG` wird fix als Variable für Parameter genommen, hier für das was hinter `-g` übergeben wurde genommen.

`optvar` bzw. `$optvar` kann man frei waehlen.

## Bedinungen (Conditionals)

Conditionals braucht es dort, wo man Code nur ausführen möchte, wenn etwas zutrifft. D.h. man überprueft eine Bedingung.

```bash
echo "Do you want to order the things?"
# Lese eine Antwort vom User ein und versorge diese in der Variable answer.
read answer
# Hier kommt eine Bedingung, dass die Antwort (answer) den Inhalt yes haben muss.
if [ "$answer" = "yes" ] 
then
  echo "Thank you, we will send them to you"
  # Hier kommt der Code der die Bestellung ausloest.
else
  echo "It is a pitty, that you do not want to get the goods"
  # Hier kommt nichts oder nachfragen beim Kunden warum jetzt nichts will...
fi
```

Was gibt es für Bedingungen? 
- Es gibt viele verschiedene Möglichkeiten den Inhalt von Variabeln oder die Eigenschaften von Files zu überprüfen. 
Die beste Beschreibung bekommt ihr wenn ihr auf eine Linux-Vm `man test` eingibt.
- Jedes Kommando das ausgeführt wird gibt eine Status-Code zurück den man als Bedingung brauchen kann.

### Beispiele für Bedingungen mit Files

**Beispiel 1:**

```bash
if [ -e /etc/cloud/cloud-init.disabled ] 
then
   echo "Cloud-init ist disabled"
   exit 1
fi
```
Falls das File /etc/cloud/cloud-init.disabled existiert soll die Ausgabe `Cloud-init ist disabled` gemacht werden und das Skript mit Exit-Code 1 verlassen werden.

**Beispiel 2:**

Überprüfen ob `/var/myapp` ein Verzeichnis ist und dieses beschreibbar, dann ein File erzeugen:
```bash
if [ -d /var/myapp ] ; then
  if [ -w /var/myapp ] ; then
    touch /var/myapp/testfile
  fi
fi
```

### Beispiele mit Variabeln

**Beispiel 1:**

Überprüfen, ob der Inhalt der Variable `myvar` grösser 100 ist:

```bash
if [ $myvar -gt 100 ] ; then
  echo "myvar ist grösser als 100"
else
  echo "myvar ist kleiner oder gleich 100"
fi
```

**Beispiel 2:**

Überprüfen ob die Variable `myvar` nichts enthält:

```bash
if [ -n $myvar ] ; then
   echo "myvar ist leer"
else
   echo "myvar enthaelt $myvar"
fi
```

### Bespiele mit Exit-Codes von Kommandos

Wenn man ein Kommando ausführt wird beim Abschluss des Kommandos ein Exit-Code gesetzt. Normalerweise ist das `0`, wenn alles gut gegangen ist. Exit-Code `0` ist also Bedingung `true` in einem Shell-Skript.
( Bei anderen Programmiersprachen wie Python, Java, ..., ist das genau umgekehrt `true` ist alles ungleich `0` )

**Beispiel 1:**

Das Kommando `mv oldfile.1 oldfile.2` wird ausgefuehrt und wenn das erfolgreich war, soll das Kommando `mv oldfile oldfile.1` ausgeführt werden:

```bash
if mv oldfile.1 oldfile.2 ; then
  mv oldfile oldfile.1
else
  echo "mv oldfile.1 oldfile.2 war nicht erfolgreich" >&2
  exit 1
fi
```
**Beispiel 2:**

Wenn der Host www.microsoft.com mit ping erreichbar ist, soll der Inhalt der Webseite mit curl runtergeladen werden:

```bash
if ping -c 1 www.microsoft.com ; then
   curl -o output.html www.microsoft.com
fi
```

## Loops

### `while` Loop

Bleibe im Loop solange Bedingung `true` ist.

**Beispiel 1:**

Warten bis meine neue VM mit der IP `10.12.32.43` erreichbar ist und dann mit ssh einloggen und `cloud-init status` ausführen:

```bash
while ! ping -c 1 10.12.32.43
do
  # Warten bis ping erfolgreich
  sleep 1
done
ssh root@10.12.32.43 "cloud-init status"
```


### `until` Loop

Wie `while` Loop aber mit umgekehrter Bedingung, d.h. wenn Bedingung `true` dann gehe aus dem Loop.

**Beispiel 1:**

Warten bis meine neue VM mit der IP `10.12.32.43` erreichbar ist und dann mit ssh einloggen und `cloud-init status` ausführen:

```bash
until ping -c 1 10.12.32.43
do
  # Warten bis ping erfolgreich
  sleep 1
done
ssh root@10.12.32.43 "cloud-init status"
```

### `for` Loop

Ein `for` Loop führt für eine Liste von Elementen den gleichen Code aus. Dabei wird das aktuelle Element in einer Variable gespeichert. D.h. der Loop

```bash
for arminsvariable in armin beat hans
do
   echo "$arminsvariable ist gross"
done
```
wird 3 Mal durchlaufen. Beim ersten Mal wird Variable `arminsvariable` mit `armin` gefüllt. Beim zweiten Mal mit `beat` und beim dritten Mal mit `hans`.
D.h. der Loop gibt folgendes aus:
```
armin ist gross
beat ist gross
hans ist gross
```

Das ist ja noch nicht so interessant. Spannend ist es wenn mann zum Beispiel für alle Files im Verzeichnis /etc/cloud/ die mit `*.txt` enden, die erste Zeile ausgeben will:

```bash
# Spezielle Option für bash setzen, sodass die Liste leer bleibt, wenn kein File das Pattern /etc/cloud/*.txt matched,also der Loop gar nicht ausgeführt wird
shopt -s nullglob
# Jetzt kommt der Loop
for file in /etc/cloud/*.txt
do
  echo "Erste Zeile von $file"
  echo "====================="
  head -1 $file
done
```

### Loops Abbrechen

Das vorzeitige Abbrechen von Loops geschieht bei allen `for`,`until` und `while`-Loops genau gleich.

**Beispiel: `break`**

Loops kann man vorzeitig abrechen, auch wenn der Loop normalerweise noch weiterlaufen würde. Dazu gibt es `break`, was den Loop beendet.

Maximal 20 Sekunden warten bis IP `10.12.32.43` erreichbar ist und dann mit ssh einloggen und `cloud-init status` ausführen:

```bash
count=0
# ping ist noch nicht ok, darum pingok=0
pingok=0
# loop bis count 0
while [ $count -lt 20 ]
do
  
  if ping -c 1 10.12.32.43 ; then
     # Wenn ping ok ist Variable pingok 1 # setzen und loop mit break verlassen
     pingok=1
     break
  fi
  # count plus 1
  let count++ 
done
if [ $pingok -eq 1 ]
then
  # Nur wenn ping erfolgreich war ssh
  ssh root@10.12.32.43 "cloud-init status"
else
  echo "ping war 20 mal nicht erfolgreich"
fi
```

**Beispiel:`continue`**
Es gibt auch noch die Möglichkeit den Rest des Codes im Loop zu überspringen. Dazu gibt es `continue`

Alle Files ausführen und den Exit-Code überprüfen, aber nur wenn diese auch ausführbar sind

```bash
for file in /var/myscripts/*.sh 
do
  if [ ! -x $file ]
  then
    # Wenn das File $file nicht ausführbar ist soll er gerade zum nächsten file springen.
    continue
  fi
  $file >/tmp/std.output 2>/tmp/std.error
  exitcode=$?
  if [ $exitcode -ne 0 ]
  then
    echo "$file war nicht erfolgreich" >&2
    echo "Der exitcode war $exitcode" >&2
    echo "Der error output war:"
    cat /tmp/std.error
  fi
done
```
