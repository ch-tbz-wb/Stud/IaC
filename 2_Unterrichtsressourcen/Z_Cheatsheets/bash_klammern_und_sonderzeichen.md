# BASH: Bedeutung von Klammern und Sonderzeichen

| Befehl | Funktionen |
| ---- | ---- |
| `()` | Kommandos als Subprozess ausführen, d.h. es kann gut mit im Hintergrund `&` ausführen kombiniert werden|
| `${}` | - Variablenname die durch `{` und `}` eingeschlossen sind <br> - Funktionen zum ersetzen und testen von Variablen (siehe `Parameter Expansion` in bash-Manpage)  |
| `$()` | Kommandos ausführen und den Output (stdout) im Skript einfügen |
| `` | genau das gleiche wie: `$()` |
| `[]` | - Bei Arrays zum referenzieren des Array-Elements (z.B. `${name[0]}`) <br> - Tests wie `[ $1 -le 10 ]` oder `[ -r file ]` die in der Manpage von `test` beschrieben sind <br> - Bei Wildcards können mögliche Buchstaben als Liste angeben werden (z.B. `dokument_[a-e].docx` matched alle Files `dokument_a.docx`, `dokument_b.docx` bis `dokument_e.docx)` |
| `[[  ]]` | Tests advanced -> Manpage von `bash` anschauen unter `CONDITIONAL EXPRESSIONS` und nach `=~` suchen |
| `~username` | Pfad von Home-Directory von `username`. Wenn kein `username` angegeben wird ist das eigene Home-Directory  |
| `&` | <li>Umleiten von Output<li> Hintergrund-Prozesse starten |
| `{1..4}` | Aufzählungen wie `file{1..10}` entspricht `file1 file2 file3 .. file10`|
| `;` | Trennt Kommandos. D.h. entspricht dem Zeilenumbruch |
| `\` | Hebt die spezielle Bedeutung des folgenden Zeichens auf. D.h. wenn man z.B. wirklich `$` als Zeichen braucht, dann benutzt man `\$` |
| `$(())` | Aritmetische Operationen. Mögliche operationen findet man unter `ARITHMETIC EVALUATION` in der `bash`-Manpage. Es gibt nur Ganzzahlen (Integer) |
| `*` und `?` | Wildcards für Filenamen-Matching `*` steht für mehrere beliebige Zeichen, `?` steht für ein beliebiges Zeichen |

Danke an Laura