# Handlungskompetenzen

## Kompetenzmatrix

| **Handlungssituation**                                           | **RP**                                            | **HZ** | **Novizenkompetenz**                                                                                                                                               | **Fortgeschrittene Kompetenz**                                                                                                                               | **Kompetenz professionellen Handelns**                                                                                                                       | **Kompetenzexpertise**                                                                                                                                                       |
|-------------------------------------------------------------------|------------------------------------------------------------------------------------------------------|-------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **a) Benutzen von Tools zur Automatisierung von Installationen in der Cloud** | B4.6, A3.3                                                                                            | 1     | Kennt grundlegende Cloud-Konzepte und kann einfache Installationen mit vorkonfigurierten Skripten oder Tools wie Ansible durchführen.                              | Kann mehrere Cloud-Plattformen bedienen und einfache Automatisierungen mit gängigen Tools wie Ansible aufsetzen.                                               | Beherrscht komplexe Installationsprozesse, schreibt eigene Ansible-Skripte und passt diese an spezifische Bedürfnisse der Cloud-Umgebung an.                                   | Entwickelt maßgeschneiderte Automatisierungslösungen, skaliert Installationen auf große Cloud-Umgebungen und optimiert die Installationsprozesse über verschiedene Clouds hinweg. |
| **b) Benutzen von Tools zur Automatisierung von Konfigurationen in der Cloud** | B12.4, A2.6                                                                                           | 2     | Kann Anleitungen befolgen, um einfache Konfigurationen in einer Cloud-Umgebung mit Tools wie Puppet oder Ansible zu automatisieren.                                  | Konfiguriert und verwaltet mehrere Cloud-Server mit Automatisierungstools. Passt Skripte an, um wiederkehrende Aufgaben zu automatisieren.                      | Automatisiert komplexe Konfigurationsprozesse, erstellt und optimiert Konfigurationsskripte und integriert sie nahtlos in CI/CD-Pipelines.                                      | Entwickelt maßgeschneiderte Lösungen für automatisierte Konfigurationen, optimiert Skalierbarkeit und sorgt für Ausfallsicherheit und Konsistenz bei Konfigurationen.           |
| **c) Evaluation dieser Tools für eine gegebene Umgebung**            | B4.7, A3.1                                                                                            | 3     | Kennt die wichtigsten Tools und kann einfache Vergleichskriterien anhand von Dokumentationen und Tutorials anwenden.                                                | Bewertet Tools anhand von vordefinierten Kriterien, führt einfache Tests durch und schlägt geeignete Lösungen für Standardumgebungen vor.                       | Führt fundierte Evaluationsprozesse durch, passt Kriterien an spezifische Umgebungsanforderungen an und plant Implementierungsstrategien.                                       | Leitet umfassende Evaluationsprozesse, erstellt maßgeschneiderte Bewertungsmethoden und gibt strategische Empfehlungen zur Toolauswahl für komplexe Cloud-Umgebungen.           |
| **d) Automatisierung von Migrationsprozessen**                       | B14.1, A3.4                                                                                           | 4     | Kennt grundlegende Migrationskonzepte und kann einfache Migrationstools nach Anleitung nutzen, z. B. für die Datenübertragung in die Cloud.                         | Automatisiert einfache Migrationsprozesse und kann diese an spezifische Anforderungen anpassen, z. B. mit Tools wie AWS Migration Hub oder CloudEndure.         | Entwickelt automatisierte Migrationslösungen, die Ausfallzeiten minimieren und die Datenintegrität sicherstellen. Beherrscht verschiedene Migrationsstrategien.                  | Gestaltet komplexe Migrationsprozesse über verschiedene Cloud-Plattformen, entwickelt maßgeschneiderte Lösungen für reibungslose Migrationen und führt optimierte Testphasen durch. |
| **e) Kombinieren von geeigneten Tools**                             | B4.6, A1.11, A3.2                                                                                     | 5     | Versteht die Funktion einzelner Automatisierungstools und kann sie im Rahmen vorgegebener Prozesse anwenden.                                                        | Kann mehrere Tools integrieren, z. B. Ansible für Konfiguration und Chef für spezifische Automatisierungsaufgaben, um einfache Prozesse zu automatisieren.     | Entwickelt und integriert mehrere Tools in eine kohärente Automatisierungslösung, die Konfiguration und CI/CD-Prozesse abdeckt.                                                  | Entwirft komplexe Tool-Chains, die individuell auf Unternehmensanforderungen zugeschnitten sind und sorgt für maximale Effizienz und Anpassungsfähigkeit der Cloud-Umgebungen.  |
| **f) Überblick über die aktuelle Toollandschaft verschaffen**         | B4.7, A2.6                                                                                             | 6     | Kennt die wichtigsten Cloud-Automatisierungstools (z. B. Ansible, Chef, Puppet) und ihre grundlegenden Funktionen.                                                  | Hat ein breites Verständnis der verfügbaren Tools und kann deren Einsatzmöglichkeiten in Standardumgebungen erklären und vergleichen.                            | Entwickelt detaillierte Analysen der aktuellen Toollandschaft, gibt fundierte Empfehlungen für den Einsatz spezifischer Tools in spezifischen Umgebungen.                       | Beherrscht die neuesten Entwicklungen in der Toollandschaft, identifiziert zukünftige Trends und optimiert den Einsatz von Tools für spezifische, innovative Anwendungsfälle.   |

* RP = Rahmenlehrplan
* HZ = Handlungsziele

---

## Allgemeine Handlungskompetenzen

* A1 Unternehmens- und Führungsprozesse gestalten und verantworten
  * A1.11 Die Motivation im Team fördern und dieses zu Höchstleistungen befähigen (Niveau: 3)

* A2 Kommunikation situationsangepasst und wirkungsvoll gestalten (Niveau: 3)
  * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren (Niveau: 3)

* A3 Die persönliche Entwicklung reflektieren und aktiv gestalten (Niveau: 3)
  * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln (Niveau: 3)
  * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren (Niveau: 3)
  * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren (Niveau: 3)
  * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln (Niveau: 3)


## Modulspezifische Handlungskompetenzen

* B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
  * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
  * B4.7 Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (Niveau: 3)

* B12 System- und Netzwerkarchitektur bestimmen
  * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)

* B14 Konzepte und Services umsetzen
  * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   

 

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
