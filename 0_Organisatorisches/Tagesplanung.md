<h1>Tagesplanungen</h1>

[toc]

# Modul IAC1

<table>
  <tr>
   <td>Tag 1
<p>
Einführung
   </td>
   <td>
<ul>

<li>Vorstellung des Zeitplans und der Arbeiten</li>

<li>Kurze Einführung</li>

<li>
Überblick
</li>

<li>[IaC-Tools](../2_Unterrichtsressourcen/A_Uebersicht_IaC/README.md)</li>

<li>[BASH Einführung](../2_Unterrichtsressourcen/C_Bash/README.md#einführungstutorial)</li>

</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 2
   </td>
   <td>
<ul>
<li>[BASH Einführung](../2_Unterrichtsressourcen/C_Bash/README.md#einführungstutorial)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 3
   </td>
   <td>
<ul>

<li>[BASH Tools](../2_Unterrichtsressourcen/C_Bash/README.md#tools)</li>

<li>Wissenstandkontrolle</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 4
   </td>
   <td>
<ul>

<li>[Aufträge: BASH Log-Funktion.](../2_Unterrichtsressourcen/C_Bash/01_Auftraege/55_logfunction.md)</li>

<li>[Aufträge: BASH Relozierbare Skripts](../2_Unterrichtsressourcen/C_Bash/01_Auftraege/50_relozierbare_skripts.md).</li>

</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 5
   </td>
   <td>
<ul>
<li>[Cloud-Init: First Steps, Logfiles, Internals von Cloudinit Part1](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-first-steps)</li>

<li>[Cloud-Init Module](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-module)</li>

<li>[Auftrag Cloud-init Wordpress](../2_Unterrichtsressourcen/D_Cloud-Init/01_Auftraege/40_apache_php_wordpress_cloudinit.md)</li>

<li>Break-Out-Sessions</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 6
   </td>
   <td>
<ul>

<li>[Aufträge: Cloud-Init Internals](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-internals-part-1)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 7
   </td>
   <td>
<ul>

<li>[Cloud-Init: Cloud-Init-Module und Base64 Encoding](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-base64-encoding)</li>

<li>[Cloud-Init: Zero-Trust](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-und-zero-trust)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 8
   </td>
   <td>
<ul>

<li>[Cloud-Init: Zero-Trust](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-und-zero-trust)</li>

<li>[Aufträge: Cloud-Init-Module (ssh, bootcmd, runcmd, ca_certs, keyboard, locale, ntp, packages, power_state, resolv_conf, vendor_data, hostname, timezone, users, write_files)](../2_Unterrichtsressourcen/D_Cloud-Init/README.md#cloud-init-standard-module)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 9
   </td>
   <td>
<ul>

<li>[Aufträge: Cloud-Init-Module (ssh, bootcmd, runcmd, ca_certs, keyboard, locale, ntp, packages, power_state, resolv_conf, vendor_data, hostname, timezone, users, write_files)](../2_Unterrichtsressourcen/D_Cloud-Init/01_Auftraege/60_cloudinit_module.md)</li>
</ul>
   </td>
  </tr>
      <tr>
   <td>Tag 10
   </td>
   <td>
<ul>
<li>[Einführung CI/CD Pipeline](../2_Unterrichtsressourcen/G_CICD/README.md#einführung)
<li>[Erste Pipeline auf Gitlab](../2_Unterrichtsressourcen/G_CICD/README.md#auftrag-erste-pipeline-auf-gitlab)
</ul>
   </td>
</tr>
<tr>
   <td>Tag 11
   </td>
   <td>
<ul>
<li>[Erste Pipeline auf Gitlab](../2_Unterrichtsressourcen/G_CICD/README.md#auftrag-erste-pipeline-auf-gitlab)
</ul>
   </td>
  </tr>
<tr>
   <td>Tag 12
   </td>
   <td>
<ul>
<li>[Theorie: Gitlab-Runner & Gitlab-Executor](../2_Unterrichtsressourcen/G_CICD/README.md#theorie-gitlab-runner--gitlab-executor)</li>
<li>[Theorie: Debug the Executor](../2_Unterrichtsressourcen/G_CICD/README.md#debug-the-executor)</li>
<li>[Handson: Debugging im Web-Terminal oder direkt in der VM](../2_Unterrichtsressourcen/G_CICD/README.md#debugging-in-webterminal-oder-direkt-in-der-vm)
</ul>
   </td>
  </tr>
<tr>
   <td>Tag 13
   </td>
   <td>
<ul>
<li>[Theorie: GITLAB-Pipelines](../2_Unterrichtsressourcen/G_CICD/README.md#theorie-gitlab-pipelines)</li>
<li>[Handson: Test-Job in deploy Stage](../2_Unterrichtsressourcen/G_CICD/README.md#hands-on-2)
</ul>
   </td>
  </tr>
</table>

# Modul IAC2

<table>  <tr>
   <td>Tag 1
   </td>
   <td>
<ul>
<li>Vorstellung des Zeitplans und der Arbeiten</li>

<li>[Ansible: Einführung Part 1: Inventory, Ad-Hoc-Commands, Modules](../2_Unterrichtsressourcen/E_Ansible/README.md#ansible-einführung)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 2
   </td>
   <td>
<ul>

<li>[Ansible: My First playbook](../2_Unterrichtsressourcen/E_Ansible/README.md#erste-schritte)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 3
   </td>
   <td>
<ul>

<li>[Ansible: My First playbook](../2_Unterrichtsressourcen/E_Ansible/README.md#erste-schritte)</li>

<li>[Ansible: Concepts](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-2--concepts)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 4

   </td>
   <td>
<ul>

<li>[Auftrag 3: Ansible Inventory erstellen](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-3--inventory)</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Tag 5
<p>
   </td>
   <td>
<ul>



<li>[Recap of Ansible: Overview](../2_Unterrichtsressourcen/E_Ansible/Overview.md)</li>
<li>[Auftrag 4: Ansible Playbook Einführung](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-4--playbooks-einführung)</li>
<li>[Auftrag 5: Ansible Playbook erstellen und benutzen](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-5--playbooks-erstellen-und-benutzen)</li>
</ul>
   </td>
  </tr>
   <tr>
   <td>Tag 6
   </td>
   <td>
<ul>
<li>[Recap of Ansible: Overview](../2_Unterrichtsressourcen/E_Ansible/Overview.md)</li>
<li>[Auftrag 4: Ansible Playbook Einführung](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-4--playbooks-einführung)</li>
<li>[Auftrag 5: Ansible Playbook erstellen und benutzen](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-5--playbooks-erstellen-und-benutzen)</li>
</ul>
   </td>
  </tr>
   <tr>
   <td>Tag 7
   </td>
   <td>
<ul>
<li>[Auftrag 6: Ansible Roles](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-6--mariadb-mit-role-aufsetzen)</li>
<li>[Auftrag 7: Ansible Inventory and AWS](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-7-aws-plugin-inventoryn)</li>
<li>[Auftrag 8: Loops und Conditionals](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-8-loops-und-conditionals)</li>
</ul>
   </td>
  </tr>
   <tr>
   <td>Tag 8
   </td>
   <td>
<ul>
<li>[Auftrag 7: Ansible Inventory and AWS](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-7-aws-plugin-inventoryn)</li>
<li>[Auftrag 8: Loops und Conditionals](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-8-loops-und-conditionals)</li>
</ul>
   </td>
  </tr>
    <tr>
   <td>Tag 9
   </td>
   <td>
<ul>
<li>[Auftrag 9: Den "output" von Tasks speichern und benutzen](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-9-den-output-von-tasks-speichern-und-benutzen)</li>
<li>[Auftrag 10: Error Handling](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-10-error-handling)</li>
<li>[Optional Auftrag 11: AWS Setup VM](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-11-aws-setup-vm)</li>
<li>[Optional Auftrag 12: Azure Setup VM](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-12-azure-setup-vm)</li>
</ul>
   </td>
  </tr>
    <tr>
   <td>Tag 10
   </td>
   <td>
<ul>
<li>[Auftrag 11: AWS Setup VM](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-11-aws-setup-vm)</li>
<li>[Auftrag 12: Azure Setup VM](../2_Unterrichtsressourcen/E_Ansible/README.md#auftrag-12-azure-setup-vm)</li>
</ul>
   </td>
</tr>
  </table>
