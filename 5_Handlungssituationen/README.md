# Handlungsziele und Handlungssituationen 

Hier sind Handlungssituationen für die jeweiligen Themen:

### 1. Benutzen von Tools zur Automatisierung von Installationen in der Cloud

Die Lernenden sind in der Lage, grundlegende Cloud-Konzepte zu verstehen und automatisierte Installationsprozesse mit geeigneten Tools wie Ansible durchzuführen.

**Handlungssituation:**  
Ein Unternehmen plant, eine neue Anwendung in der Cloud zu deployen. Der IT-Administrator muss mehrere Instanzen dieser Anwendung auf verschiedenen Cloud-Umgebungen (z. B. AWS, Azure und Google Cloud) installieren. Um den Prozess effizient und fehlerfrei zu gestalten, nutzt der Administrator Tools wie **Terraform** oder **Ansible**. Mit Terraform werden die notwendigen Infrastrukturressourcen (wie virtuelle Maschinen, Netzwerke und Speicher) bereitgestellt, und mit Ansible werden Softwarepakete und Abhängigkeiten auf den Instanzen installiert. Der Administrator erstellt wiederverwendbare Skripte, um diese Prozesse zu automatisieren und auf Knopfdruck neue Installationen durchzuführen.

### 2. Benutzen von Tools zur Automatisierung von Konfigurationen in der Cloud

Die Lernenden können Tools wie Puppet oder Ansible verwenden, um Konfigurationen in einer Cloud-Umgebung effizient zu automatisieren.

**Handlungssituation:**  
Ein DevOps-Team steht vor der Herausforderung, hunderte von Servern in einer Hybrid-Cloud-Umgebung zu konfigurieren. Dabei sollen Betriebssystem-Einstellungen, Sicherheitsrichtlinien und Anwendungskonfigurationen konsistent angewendet werden. Das Team entscheidet sich für **Chef** und **Puppet** zur Automatisierung der Konfigurationen. Sie erstellen und testen sogenannte „Cookbooks“ bzw. „Manifests“, die sicherstellen, dass alle Server in der Umgebung dieselben Konfigurationen und Sicherheitsstandards erhalten. Dadurch wird die manuelle Konfiguration minimiert und menschliche Fehler werden vermieden.

### 3. Evaluation dieser Tools für eine gegebene Umgebung
Die Lernenden sind fähig, Automatisierungstools zu bewerten und passende Lösungen für spezifische Anforderungen einer Cloud-Umgebung auszuwählen.

**Handlungssituation:**  
Ein mittelständisches Unternehmen steht vor der Entscheidung, ob es seine Cloud-Operations automatisieren soll, um effizienter zu werden. Der IT-Leiter erhält die Aufgabe, verschiedene Automatisierungstools wie **Ansible**, **Terraform**, **Pulumi** und **SaltStack** zu evaluieren. Dafür prüft er die Anforderungen der bestehenden Cloud-Umgebung und erstellt eine Liste von Kriterien wie Plattformkompatibilität, Lernkurve, Integrationsmöglichkeiten und Kosten. Er führt Tests in der vorhandenen Cloud-Infrastruktur durch, um zu ermitteln, welches Tool am besten zu den Bedürfnissen des Unternehmens passt.

### 4. Automatisierung von Migrationsprozessen

Die Lernenden verstehen die Konzepte der Datenmigration und können Migrationsprozesse automatisieren, um Ausfallzeiten zu minimieren und Datenintegrität sicherzustellen.

**Handlungssituation:**  
Ein Unternehmen plant, seine on-premise IT-Infrastruktur in die Cloud zu migrieren. Der IT-Projektmanager wird beauftragt, den Migrationsprozess zu automatisieren, um Ausfallzeiten zu minimieren. Er entscheidet sich für **CloudEndure** zur Automatisierung des Datenmigrationsprozesses und **AWS Migration Hub**, um den Fortschritt der Migration zu überwachen. Der Migrationsprozess umfasst die Erfassung der bestehenden Serverkonfigurationen, das Erstellen von Sicherungskopien und deren Übertragung in die Cloud-Umgebung. Durch Automatisierung wird die Migration innerhalb eines vordefinierten Zeitfensters abgeschlossen, ohne die täglichen Geschäftsabläufe zu stören.

### 5. Kombinieren von geeigneten Tools
Die Lernenden sind in der Lage, verschiedene Automatisierungstools zu integrieren, um umfassende Lösungen für die Automatisierung von Cloud-Prozessen zu schaffen.

**Handlungssituation:**  
Ein Unternehmen führt verschiedene Tools zur Automatisierung seiner Cloud-Operationen ein, darunter **Terraform** zur Bereitstellung von Infrastruktur, **Jenkins** für CI/CD-Pipelines und **Ansible** zur Konfigurationsverwaltung. Der DevOps-Engineer wird beauftragt, diese Tools zu kombinieren, um eine nahtlose Automatisierung von Infrastruktur- und Anwendungsbereitstellungen zu gewährleisten. Er erstellt eine Jenkins-Pipeline, die zuerst Terraform-Skripte ausführt, um die benötigte Infrastruktur bereitzustellen, und anschließend Ansible-Playbooks, um die Server zu konfigurieren. Dadurch werden komplexe Prozesse wie Infrastrukturmanagement und Software-Deployment stark vereinfacht und automatisiert.

### 6. Einen Überblick über die aktuelle Toollandschaft verschaffen
Die Lernenden können die wichtigsten Cloud-Automatisierungstools und deren Einsatzmöglichkeiten analysieren und fundierte Empfehlungen zur Toolauswahl geben.

**Handlungssituation:**  
Ein IT-Berater wird von einem Unternehmen gebeten, einen umfassenden Überblick über die aktuellen Cloud-Automatisierungstools zu erstellen. Dazu analysiert er den Markt und identifiziert relevante Tools wie **Terraform**, **CloudFormation**, **Pulumi**, **Chef**, **Puppet**, **Ansible** und **SaltStack**. Der Berater erstellt einen Bericht, der die Vor- und Nachteile jedes Tools, deren Einsatzmöglichkeiten, Integrationen und zukünftige Entwicklungen beleuchtet. Basierend auf den Anforderungen des Unternehmens gibt er Empfehlungen für die Einführung geeigneter Tools, um deren Cloud-Umgebung effizienter zu gestalten.

Diese Handlungssituationen bieten reale Szenarien, in denen Automatisierungstools in der Cloud eingesetzt werden.
